from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
"LFN:/lhcb/user/i/ismith/2016_02/126039/126039872/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040001/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040026/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040050/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040055/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040060/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040065/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040070/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040073/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040079/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040094/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040147/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040196/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040202/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040215/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040225/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040231/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040240/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040248/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040254/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040261/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040309/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040379/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040386/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040392/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040400/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040404/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040411/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040420/Gauss-13774011-5ev-20160209.xgen",
"LFN:/lhcb/user/i/ismith/2016_02/126040/126040428/Gauss-13774011-5ev-20160209.xgen"], clear=True)
