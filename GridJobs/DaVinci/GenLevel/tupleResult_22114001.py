from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple,
    LoKi__Hybrid__TupleTool
)
from DecayTreeTuple.Configuration import *

#import cppyy
#cppyy.gbl.gSystem.Load('libLHCbKernelDict.so')


"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
# [B_s0 -> (phi(1020) -> K+ K-) K+ K-]cc
#decay = "[B+ -> ^(phi(1020) -> ^K+ ^K-) ^mu+ ^nu_mu]CC"
decay = "[D0 -> ^phi(1020) ^mu+ ^mu-]CC"
decay_heads = ["D0", "D~0"]
year = 2012

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
mctuple.ToolList = [
    "MCTupleToolHierarchy",
    "LoKi::Hybrid::MCTupleTool/LoKi_Photos"#,
    #"LoKi::Hybrid::TupleTool/LoKiTool"
]
# Add a 'number of photons' branch
mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True

mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
    "nPhotos": "MCNINTREE(('gamma' == MCABSID))",
    "MC_PT"    : "MCPT",
    "MC_THETA" : "MCTHETA",
    "MC_ETA"   : "MCETA",
    "MC_PHI"   : "MCPHI",
    "MC_ABSID" : "MCABSID"

}


mctuple.addTupleTool('TupleToolPrimaries')


# Print the decay tree for any particle in decay_heads
printMC = PrintMCTree()
printMC.ParticleNames = decay_heads

# Configure DaVinci
DaVinci().TupleFile = "DVntuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
DaVinci().UserAlgorithms = [printMC, mctuple]
#DaVinci().UserAlgorithms = [mctuple]
