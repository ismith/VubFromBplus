import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import Selection

#############################################################################################
#                                                                                           #
#                              Define Daughter Cuts here:                                   #
#                                                                                           #
#############################################################################################
"""
Mu_Cuts = {	"mu+"	:"(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.3)& (PIDmu-PIDpi> 0.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 ) & (MIPCHI2DV(PRIMARY)> 12 )",
		"mu-"	:"(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.3)& (PIDmu-PIDpi> 0.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 ) & (MIPCHI2DV(PRIMARY)> 12 )"
}

K_Cuts = {	"K+"	:"(TRCHI2DOF < 2.2 )& (P> 3000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.3)& (PIDK-PIDpi> 0.0 )& (PIDK-PIDp> -2.0 )& (PIDK-PIDmu> -2.0 ) & (MIPCHI2DV(PRIMARY)> 16 )",
		"K-"	:"(TRCHI2DOF < 2.2 )& (P> 3000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.3)& (PIDK-PIDpi> 0.0 )& (PIDK-PIDp> -2.0 )& (PIDK-PIDmu> -2.0 ) & (MIPCHI2DV(PRIMARY)> 16 )"
}

"""
Mu_Cuts = {	"mu+"	:"(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.3)& (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 )& (MIPCHI2DV(PRIMARY)> 12 )",
		"mu-"	:"(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.3)& (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 )& (MIPCHI2DV(PRIMARY)> 12 )"
}

K_Cuts = {	"K+"	:"(TRCHI2DOF < 2.2 )& (P> 3000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.3)& (PIDK-PIDpi> 0.0 )& (PIDK-PIDp> -2.0 )& (PIDK-PIDmu> -2.0 ) & (MIPCHI2DV(PRIMARY)> 16 )",
		"K-"	:"(TRCHI2DOF < 2.2 )& (P> 3000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.3)& (PIDK-PIDpi> 0.0 )& (PIDK-PIDp> -2.0 )& (PIDK-PIDmu> -2.0 ) & (MIPCHI2DV(PRIMARY)> 16 )"
}


Mu_Cuts_Loose={	"mu+"	:"ALL",
		"mu-"	:"ALL"
}

#############################################################################################
#                                                                                           #
#                              Define Combination Cuts here:                                #
#                                                                                           #
#############################################################################################
"""
KK_Comb_Cut = "(AM > 980*MeV) & (AM< 1060.0*MeV)"

PhiMu_Comb_Cut = "(AM>2000.0*MeV) & (AM<5500.0*MeV)"
"""

KK_Comb_Cut = "(AM > 980*MeV) & (AM< 1060.0*MeV)"

PhiMu_Comb_Cut = "(AM>2000.0*MeV) & (AM<5500.0*MeV)"

BplusMu_Comb_Cut = "(AM>4800.0*MeV) & (AM<5700.0*MeV)"

#############################################################################################
#                                                                                           #
#                              Define Mother Cuts here:                                     #
#                                                                                           #
#############################################################################################


"""
KK_Mother_Cut = "(MM > 990*MeV) & (MM < 1050*MeV) & (PT > 500.0 *MeV) & (VFASPF(VCHI2/VDOF) < 8 ) & (MIPCHI2DV(PRIMARY)> 6 ) & (BPVDIRA> 0.9)"

PhiMu_Mother_Cut = "(MM > 2100*MeV) & (BPVCORRM>2500.0*MeV) & (VFASPF(VCHI2/VDOF)< 5.0) & (BPVDIRA> 0.99) & (BPVVDCHI2 >30.0)"
"""

KK_Mother_Cut = "(MM > 990*MeV) & (MM < 1050*MeV) & (VFASPF(VCHI2/VDOF) < 6 ) & (PT > 500.0 *MeV) & (MIPCHI2DV(PRIMARY)> 9 ) & (BPVDIRA> 0.9)"

PhiMu_Mother_Cut = "(MM > 2100*MeV) & (BPVCORRM>2500.0*MeV) & (VFASPF(VCHI2/VDOF)< 3.0) & (BPVDIRA> 0.994)& (BPVVDCHI2 >50.0)"

BplusMu_Mother_Cut = "(MM>4900.0*MeV) & (MM<5600.0*MeV) & ( abs( sqrt( (CHILD(E,1,2) + CHILD(E,2))**2 - (CHILD(PX,1,2) + CHILD(PX,2))**2 - (CHILD(PY,1,2) + CHILD(PY,2))**2 - (CHILD(PZ,1,2) + CHILD(PZ,2))**2 )-3096)<100*MeV)"

#############################################################################################
#                                                                                           #
#                              Now start building the selection                             #
#                                                                                           #
#############################################################################################


from Configurables import CombineParticles
_Phi2KK = CombineParticles("Phi2KK",
	DecayDescriptor	= "phi(1020) -> K+ K-",
	DaughtersCuts	= K_Cuts,
	CombinationCut	= KK_Comb_Cut,
	MotherCut		= KK_Mother_Cut
)

from StandardParticles import StdLooseKaons
Sel_Phi2KK = Selection("Sel_Phi2KK", 
                              Algorithm = _Phi2KK, 
                              RequiredSelections = [StdLooseKaons])


_bu2PhiMu = CombineParticles("Bu2PhiMu",
	DecayDescriptor	= "[B+ -> phi(1020) mu+]cc",
	DaughtersCuts	= Mu_Cuts,
	CombinationCut	= PhiMu_Comb_Cut,
	MotherCut		= PhiMu_Mother_Cut
)
from StandardParticles import StdLooseMuons
Sel_Bu2PhiMu = Selection("Sel_Bu2PhiMu", 
                              Algorithm = _bu2PhiMu, 
                              RequiredSelections = [StdLooseMuons, Sel_Phi2KK])


_bs2buMu = CombineParticles("Bs2BuMu",
	DecayDescriptor	= "[B_1(L)0 -> B+ mu-]cc",
	DaughtersCuts	= Mu_Cuts_Loose,
	CombinationCut	= BplusMu_Comb_Cut,
	MotherCut	= BplusMu_Mother_Cut
)


Sel_bs2buMu = Selection("Sel_Bs2BuMu", 
	Algorithm = _bs2buMu, 
	RequiredSelections = [ StdLooseMuons, Sel_Phi2KK,  Sel_Bu2PhiMu]
)



from Configurables import LoKi__HDRFilter as StripFilter
stripping_filter = StripFilter ( 'StrippingPass' , Code = "HLT_PASS('StrippingFullDSTDiMuonJpsi2MuMuDetachedLineDecision')" , Location = "/Event/Strip/Phys/DecReports")

from PhysSelPython.Wrappers import SelectionSequence
JpsiPhi_sequence = SelectionSequence('Seq_Bs2BuMu', EventPreSelector = [stripping_filter], TopSelection = Sel_bs2buMu)
#JpsiPhi_sequence = SelectionSequence('Seq_Bs2BuMu', TopSelection = Sel_bs2buMu)

from Configurables import DaVinci, DecayTreeTuple
DaVinci().UserAlgorithms += [ JpsiPhi_sequence.sequence() ]


