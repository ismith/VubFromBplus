import sys
#sys.path.append('/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction')

#from GetDSTs import findDSTs




def SubmitBplusPhiMuNu_MC( eventNumber, JpsiPhi = False, prod_filter = "*", batch_submit = False):
	from fnmatch import fnmatch

	Application = DaVinci()
	Application.version = 'v38r1p1'


	JobName = ""
	datafiles = []
	DSTs = []
	nfiles = 5


	if len( eventNumber ) == 8:
		try:
			eventType = int( eventNumber )
		except:
			print "eventType doesn't make sense"
			exit()
		#from GetDSTs import findDSTs
		#DSTs = findDSTs( eventType )
		
		f = open('MC_DATA.conf', 'r')

		for line in f:
			if line == '\n':
				continue
			if line[0] == '#':
				continue
			line_data = line.split()
			if line_data[0] == eventNumber:
				if fnmatch( line_data[1], prod_filter):
					DSTs.append(line_data[1:])
					print line
			 
		
		
		ndfiles = len( DSTs )
		
		#for job_number in range(ndfiles):
		#	print DSTs[job_number][0]
	else:
		print "Event Type needs to be 8 digits"
		exit()

	

	for job_number in range(ndfiles):

		
		ds = []
		#for filename in datafiles:
		bk_query = BKQuery( path =  DSTs[job_number][0] )
		ds.extend( bk_query.getDataset() )
		
		Splitter = SplitByFiles(filesPerJob = nfiles, maxFiles = -1, ignoremissing = True, bulksubmit=False)
		Output   = [ DiracFile('*.root'), LocalFile('summary.xml') ]
		Backend  = Dirac()
		#Input    = [ File ( './LFNs.py' ) ]
		Options = [];
		if ( JpsiPhi ):
			JobName = "MCJpsiPhi "
			Options = [ File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/ntuple_optionsrestrip24.py" ),
				    File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/JpsiPhi_selection.py" ),
				    File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/JpsiPhi_DecayTreeBuilder.py") 
				  ]
		else:
			Options = [ File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/ntuple_optionsrestrip24.py" ),
				    File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_restripping.py" ),
				    File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_selection.py" ),
				    File ( "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_DecayTreeBuilder.py") 
				  ]
		
		
		
		Application.optsfile = Options
		j = Job ()
		j.name         = JobName + eventNumber + " AutoSubmission"
		j.comment      = eventNumber + " " + DSTs[job_number][0] 
		j.application  = Application
		
		j.application.user_release_area = '/afs/cern.ch/user/i/ismith/cmtuser'
		j.splitter     = Splitter
		j.backend      = Backend
		j.outputfiles  = Output
		j.inputdata  = ds
		j.do_auto_resubmit = True
		j.application.extraopts = ""
		j.application.extraopts += "DaVinci().DDDBtag   = '" + DSTs[job_number][1] + "'" + "\n"
		j.application.extraopts += "DaVinci().CondDBtag   = '" + DSTs[job_number][2] + "'" + "\n"
		j.application.extraopts += "DaVinci().EvtMax   = -1" + "\n"
		
		
		#Fill in the correct information for the year
		if fnmatch( DSTs[job_number][0], "/MC/2011/*"):
			j.application.extraopts += "DaVinci().DataType = '2011'" + "\n"
			
		elif fnmatch( DSTs[job_number][0], "/MC/2012/*"):
			j.application.extraopts += "DaVinci().DataType = '2012'" + "\n"
			
		elif fnmatch( DSTs[job_number][0], "/MC/2015/*"):
			j.application.extraopts += "DaVinci().DataType = '2015'" + "\n"
			
		elif fnmatch( DSTs[job_number][0], "/MC/2016/*"):
			j.application.extraopts += "DaVinci().DataType = '2016'" + "\n"
			
		if batch_submit:
			queues.add( j.submit )
		

def SearchProductions( eventType ):
	from GetDSTs import findDSTs
	x = findDSTs(eventType)
	
	

def SubmitBplusPhiMuNu_Data(year, batch_submit = False, FakeMu = False):

	JobName = ""
	datafiles = []

	nfiles = 80


	JobName = "'B+PhiMuNu Data "
	if FakeMu:
		JobName = "'B+PhiMuNu FakeMu "	


	j = Job ()
	j.application  = DaVinci()
	j.application.version = 'v38r1p1'
	j.application.extraopts = ""
	if year == 2015:
		JobName += '2015'
		datafiles.append( '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/SEMILEPTONIC.DST' )
		datafiles.append( '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24/90000000/SEMILEPTONIC.DST' )
		j.application.extraopts += "DaVinci().DataType = '2015'" + "\n"

	elif year == 2012:
		JobName += '2012'
		datafiles.append( '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST' )
		datafiles.append( '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST' )
		j.application.extraopts += "DaVinci().DataType = '2012'" + "\n"
		
	elif year == 2011:
		JobName += '2011'
		datafiles.append( '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p1a/90000000/SEMILEPTONIC.DST' )
		datafiles.append( '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1p1a/90000000/SEMILEPTONIC.DST' )
		j.application.extraopts += "DaVinci().DataType = '2011'" + "\n"
		
	elif year == 2016:
		JobName += '2016'
		datafiles.append( '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping26/90000000/SEMILEPTONIC.DST' )
		datafiles.append( '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping26/90000000/SEMILEPTONIC.DST' )
		j.application.extraopts += "DaVinci().DataType = '2016'" + "\n"
				
		
	ds = []
	for filename in datafiles:
		bk_query = BKQuery( path =  filename )
		ds.extend( bk_query.getDataset() )
		


	Splitter = SplitByFiles(filesPerJob = nfiles, maxFiles = -1, ignoremissing = True, bulksubmit=False)
	Output   = [ DiracFile('*.root'), LocalFile('summary.xml') ]
	Backend  = Dirac()
	#Input    = [ File ( './LFNs.py' ) ]
	Options = [ "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/ntuple_options_data.py",
	            "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_selection.py",
	            "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_DecayTreeBuilder.py"]
	
	if FakeMu:
		Options = [ "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/ntuple_options_data.py",
			    "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_Fake_selection.py",
			    "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/phimunu_DecayTreeBuilder.py"]

	j.application.optsfile = Options
	j.name         = JobName
	j.application.user_release_area = '/afs/cern.ch/user/i/ismith/cmtuser'
	j.application.extraopts += "DaVinci().EvtMax   = -1" + "\n"
	j.splitter     = Splitter
	j.backend      = Backend
	j.outputfiles  = Output
	j.inputdata  = ds
	j.do_auto_resubmit = True
	if batch_submit:
		queues.add( j.submit )

def SubmitBplusPhiMuNu_Data_JPsiPhi(year, batch_submit = False):

	JobName = ""
	datafiles = []

	nfiles = 80


	JobName = ""
	
	j = Job ()
	j.application  = DaVinci()
	j.application.version = 'v38r1p1'

	j.application.extraopts = "" + "\n"
	if year == 2015:
		JobName = 'Bs-JpsiPhi Data 2015'
		datafiles.append( '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/DIMUON.DST' )
		datafiles.append( '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24/90000000/DIMUON.DST' )
		j.application.extraopts += "DaVinci().DataType = '2015'" + "\n"

	elif year == 2012:
		JobName = 'Bs-JpsiPhi Data 2012'
		datafiles.append( '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST' )
		datafiles.append( '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/DIMUON.DST' )
		j.application.extraopts += "DaVinci().DataType = '2012'" + "\n"
		
	elif year == 2011:
		JobName = 'Bs-JpsiPhi Data 2011'
		datafiles.append( '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20r1/90000000/DIMUON.DST' )
		datafiles.append( '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20r1/90000000/DIMUON.DST' )
		j.application.extraopts += "DaVinci().DataType = '2011'" + "\n"
		
	elif year == 2016:
		JobName = 'Bs-JpsiPhi Data 2016'
		datafiles.append( '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping26/90000000/DIMUON.DST' )
		datafiles.append( '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping26/90000000/DIMUON.DST' )
		j.application.extraopts += "DaVinci().DataType = '2016'" + "\n"
				
		
	ds = []
	for filename in datafiles:
		bk_query = BKQuery( path =  filename )
		ds.extend( bk_query.getDataset() )
		


	Splitter = SplitByFiles(filesPerJob = nfiles, maxFiles = -1, ignoremissing = True, bulksubmit=False)
	Output   = [ DiracFile('*.root'), LocalFile('summary.xml') ]
	Backend  = Dirac()
	#Input    = [ File ( './LFNs.py' ) ]
	Options = [ "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/ntuple_options_data.py",
	            "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/JpsiPhi_selection.py",
	            "/afs/cern.ch/user/i/ismith/ANA/VubFromBplus/GridJobs/DaVinci/NTupleProduction/JpsiPhi_DecayTreeBuilder.py"]
	
	j.application.optsfile = Options
	j.name         = JobName
	j.application.user_release_area = '/afs/cern.ch/user/i/ismith/cmtuser'
	j.application.extraopts += "DaVinci().EvtMax   = -1" + "\n"
	j.splitter     = Splitter
	j.backend      = Backend
	j.outputfiles  = Output
	j.inputdata  = ds
	j.do_auto_resubmit = True
	if batch_submit:
		queues.add( j.submit )

print "Usage is as follows:"
print "\tSubmitBplusPhiMuNu_MC( 8-Digit-Event-Type, Wildcard-To-Select-BKPaths Default is '*' )"
print "For Example, to run over signal:"
print "\tSubmitBplusPhiMuNu_MC(\"12513010\")"
print "To Run over Bs->J/psi phi:"
print "\tSubmitBplusPhiMuNu_MC(\"13144001\")"
print ""
print "To See All the Productions for a given event type do:"
print "\tSearchProductions( \"XXXXXXXX\" )"
print ""
print "To Run over data use:"
print "\tSubmitBplusPhiMuNu_Data(year)"

#queues.add(SubmitBplusPhiMuNu_Data, args=[2011, True])
#queues.add(SubmitBplusPhiMuNu_Data, args=[2012, True])
#queues.add(SubmitBplusPhiMuNu_Data, args=[2015, True])
#queues.add(SubmitBplusPhiMuNu_Data, args=[2016, True])
#SubmitBplusPhiMuNu_Data(2012, True)
#SubmitBplusPhiMuNu_Data(2015, True)
#SubmitBplusPhiMuNu_MC("12513010", "*", True)
