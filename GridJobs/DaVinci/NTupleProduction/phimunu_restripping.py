
from StrippingConf.Configuration import StrippingConf, StrippingStream


#############################################################################################
#                                                                                           #
#                 Define Variables such as line, stream stripping version                   #
#                                                                                           #
#############################################################################################

Stripping_Version = 'stripping24'
Stripping_Stream = 'MyStream'
Stripping_Line = 'StrippingB2XuMuNuB2PhiLine'


from Configurables import EventNodeKiller
phimunu_EN_killer = EventNodeKiller()
phimunu_EN_killer.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

from StrippingSettings.Utils import strippingConfiguration
config  = strippingConfiguration(Stripping_Version)

from StrippingArchive import strippingArchive
archive = strippingArchive(Stripping_Version)

from StrippingArchive.Utils import buildStreams
streams = buildStreams(stripping=config, archive=archive) 

from StrippingConf.Configuration import StrippingStream
MyStream = StrippingStream(Stripping_Stream)
MyLines = [ Stripping_Line ]

for stream in streams: 
    for line in stream.lines:
        if line.name() in MyLines:
            MyStream.appendLines( [ line ] ) 

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()


from StrippingConf.Configuration import StrippingConf
phimunu_restripping = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )
                    
from Configurables import DaVinci, DecayTreeTuple

DaVinci().appendToMainSequence( [ phimunu_EN_killer, phimunu_restripping.sequence() ] )   # Kill old stripping banks first
                    
                    
