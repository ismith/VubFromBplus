from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple.Configuration import *

#############################################################################################
#                                                                                           #
#                                  Create The DecayTree                                     #
#                                                                                           #
#############################################################################################
phimunu_DecayTree = DecayTreeTuple('TupleBu2phimunu')

phimunu_DecayTree.Decay = '[B+ -> ^(phi(1020) -> ^K+ ^K-) ^mu+ ]CC'


phimunu_DecayTree.addBranches({'Bplus' : '^[B+ -> (phi(1020) -> K+ K-) mu+ ]CC',
					'Phi' : '[B+ -> ^(phi(1020) -> K+ K-) mu+ ]CC',
					'Kplus' : '[B+ -> (phi(1020) -> ^K+ K-) mu+ ]CC',
 					'Kminus' : '[B+ -> (phi(1020) -> K+ ^K-) mu+ ]CC',
 					'muplus' : '[B+ -> (phi(1020) -> K+ K-) ^mu+ ]CC',
               })

phimunu_DecayTree.Inputs = [ "Phys/Sel_Bu2PhiMu/Particles" ]

#############################################################################################
#                                                                                           #
#             Add The TupleTools that require no configuration                              #
#                                                                                           #
#############################################################################################


phimunu_DecayTree.addTupleTool('TupleToolPrimaries')
phimunu_DecayTree.addTupleTool("TupleToolRecoStats")

"""
from Configurables import TupleToolIsoBDT
phimunu_DecayTree.Kplus.addTupleTool("TupleToolIsoBDT")
phimunu_DecayTree.Kminus.addTupleTool("TupleToolIsoBDT")
phimunu_DecayTree.muplus.addTupleTool("TupleToolIsoBDT")

from Configurables import TupleToolIsoMuon
phimunu_DecayTree.Kplus.addTupleTool("TupleToolIsoMuon")
phimunu_DecayTree.Kminus.addTupleTool("TupleToolIsoMuon")
"""
from Configurables import TupleToolIsoGeneric, LoKi__Hybrid__TupleTool

TupleToolIsoGeneric =  phimunu_DecayTree.addTupleTool("TupleToolIsoGeneric")
TupleToolIsoGeneric.Verbose = True

MCTruth_noniso           = TupleToolMCTruth('MCTruth_noniso')
MCTruth_noniso.ToolList += ["MCTupleToolHierarchy" ]
LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
LoKiTool_noniso.Variables = {
    "ETA"              : "ETA",
    "PHI"              : "PHI"
    }
TupleToolIsoGeneric.ToolList += [ "TupleToolTrackInfo" , "LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
TupleToolIsoGeneric.addTool(LoKiTool_noniso )


#############################################################################################
#                                                                                           #
#             Add The TupleTools that require simple configuration                          #
#                                                                                           #
#############################################################################################


track_tool = phimunu_DecayTree.addTupleTool('TupleToolTrackInfo')
track_tool.Verbose = True

phimunu_DecayTree.addTupleTool('TupleToolSLTools')
phimunu_DecayTree.TupleToolSLTools.Bmass = 5279.17





#############################################################################################
#                                                                                           #
#        Add The TupleTools that require more advanced configuration                        #
#                                                                                           #
#############################################################################################




Head_Variables = {
		"VCHI2NDOF"			: "VFASPF(VCHI2/VDOF)",
		"ETA"					: "ETA"
   }
   
Bplus_Variables = {
		"M_CORR"				: "BPVCORRM",
		"BPVDIRA"				: "BPVDIRA",
		"BPVVDCHI2"			: "BPVVDCHI2",
		"MIPCHI2DV_PRIMARY"	: "MIPCHI2DV(PRIMARY)",
		"WrongMass_PhiK"		: "WM( 'phi(1020)' , 'K+')",
		"WrongMass_PhiPi"		: "WM( 'phi(1020)' , 'pi+')",
		"WrongMass_D0K"			: "WM( 'D0' , 'K+')",
		"WrongMass_D0Pi"		: "WM( 'D0' , 'pi+')",
		"WrongMass_DsK"			: "WM( 'D_s-' , 'K+')",
		"WrongMass_DsPi"		: "WM( 'D_s-' , 'pi+')"

}

Phi_Variables = {
		"BPVDIRA"			: "BPVDIRA",
		"BPVVDCHI2"			: "BPVVDCHI2",
		"MIPCHI2DV_PRIMARY"	: "MIPCHI2DV(PRIMARY)",
		"WrongMass_Kpi"		: "WM( 'K+' , 'pi-')",
		"WrongMass_piK"		: "WM( 'pi+' , 'K-')",
		"WrongMass_piK"		: "WM( 'mu+' , 'mu-')",
		"WrongMass_pipi"	: "WM( 'pi+' , 'pi-')"
}

Track_Variables = {
		"TRCHI2DOF"			: "TRCHI2DOF"
}


LoKiTool = phimunu_DecayTree.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
LoKiTool.Variables = Head_Variables


LokiTool_Bplus = phimunu_DecayTree.Bplus.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
LokiTool_Bplus.Variables = Bplus_Variables

LokiTool_Phi = phimunu_DecayTree.Phi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
LokiTool_Phi.Variables = Phi_Variables

LokiTool_Kplus = phimunu_DecayTree.Kplus.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
LokiTool_Kplus.Variables = Track_Variables

LokiTool_Kminus = phimunu_DecayTree.Kminus.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
LokiTool_Kminus.Variables = Track_Variables

LokiTool_muplus = phimunu_DecayTree.muplus.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
LokiTool_muplus.Variables = Track_Variables



from Configurables import TupleToolTISTOS

phimunu_DecayTree.Bplus.addTupleTool('TupleToolTISTOS/TupleToolTISTOS')
phimunu_DecayTree.Bplus.TupleToolTISTOS.VerboseL0 = True
phimunu_DecayTree.Bplus.TupleToolTISTOS.VerboseHlt1 = True
phimunu_DecayTree.Bplus.TupleToolTISTOS.VerboseHlt2 = True             
phimunu_DecayTree.Bplus.TupleToolTISTOS.TriggerList=[
   "Hlt2TopoMu4BodyBBDTDecision",
   "L0HadronDecision",
   "L0MuonDecision",
   "L0DiMuonDecision",
   "Hlt2SingleMuonDecision",
   "Hlt2TopoMu2BodyBBDTDecision",
   "Hlt2TopoMu3BodyBBDTDecision",
   "Hlt2IncPhiDecision",
   "Hlt1TrackMuonDecision",
   "Hlt2MuTrackDecision",
   "Hlt2Topo3BodySimpleDecision",  
   "Hlt1TrackAllL0Decision",
   "Hlt2SingleMuonHighPTDecision",
   "Hlt2SingleMuonLowPTDecision",
   "Hlt2IncPhiDecision"
   #"Hlt2TFBc2JpsiMuXDecision",
   #"Hlt2TFBc2JpsiMuXSignalDecision",
   #"Hlt2Topo2BodySimpleDecision",
   #"Hlt2B2HHPi0_MergedDecision",
   #"Hlt2CharmHadD2HHHDecision",
   #"Hlt2Topo4BodySimpleDecision",
   #"Hlt2CharmHadD2HHHWideMassDecision",
   #"Hlt2DiMuonJPsiHighPTDecision",
   #"Hlt2DiMuonBDecision",
   #"Hlt2Topo4BodyBBDTDecision",
   #"Hlt2DiMuonZDecision",
   #"Hlt2DiMuonDetachedDecision",
   #"Hlt2DiMuonDetachedHeavyDecision",
   #"Hlt2DiMuonDetachedJPsiDecision",
   #"Hlt2TriMuonDetachedDecision",
   #"Hlt2TopoE3BodyBBDTDecision",
   #"Hlt2TriMuonTauDecision",
   #"Hlt2TopoE4BodyBBDTDecision",
   #"Hlt2CharmHadD02HHHHDecision",
   #"Hlt2CharmHadD02HHHHWideMassDecision",
   #"Hlt2CharmHadD02HHKsLLDecision",
   #"Hlt2B2HHLTUnbiasedDecision",
   #"Hlt2Dst2PiD02PiPiDecision",
   #"Hlt2CharmHadD02HH_D02PiPiDecision",
   #"Hlt2Dst2PiD02MuMuDecision",
   #"Hlt2CharmHadD02HH_D02PiPiWideMassDecision",
   #"Hlt2Dst2PiD02KMuDecision",
   #"Hlt2CharmHadD02HH_D02KKDecision",    
   
   ]


""" //Ignore cone isolation while we look at track isolation
from Configurables import TupleToolConeIsolation
phimunu_DecayTree.Bplus.addTupleTool("TupleToolConeIsolation")
phimunu_DecayTree.Bplus.TupleToolConeIsolation.FillAsymmetry = True
phimunu_DecayTree.Bplus.TupleToolConeIsolation.FillDeltas = True
phimunu_DecayTree.Bplus.TupleToolConeIsolation.FillComponents = True
phimunu_DecayTree.Bplus.TupleToolConeIsolation.MinConeSize = 0.5
phimunu_DecayTree.Bplus.TupleToolConeIsolation.SizeStep    = 0.5
phimunu_DecayTree.Bplus.TupleToolConeIsolation.MaxConeSize = 2.0

phimunu_DecayTree.Phi.addTupleTool("TupleToolConeIsolation")
phimunu_DecayTree.Phi.TupleToolConeIsolation.FillAsymmetry = True
phimunu_DecayTree.Phi.TupleToolConeIsolation.FillDeltas = True
phimunu_DecayTree.Phi.TupleToolConeIsolation.FillComponents = True
phimunu_DecayTree.Phi.TupleToolConeIsolation.MaxConeSize = 1.0

phimunu_DecayTree.Kminus.addTupleTool("TupleToolConeIsolation")
phimunu_DecayTree.Kminus.TupleToolConeIsolation.FillAsymmetry = True
phimunu_DecayTree.Kminus.TupleToolConeIsolation.FillDeltas = True
phimunu_DecayTree.Kminus.TupleToolConeIsolation.FillComponents = True
phimunu_DecayTree.Kminus.TupleToolConeIsolation.MaxConeSize = 1.0

phimunu_DecayTree.Kplus.addTupleTool("TupleToolConeIsolation")
phimunu_DecayTree.Kplus.TupleToolConeIsolation.FillAsymmetry = True
phimunu_DecayTree.Kplus.TupleToolConeIsolation.FillDeltas = True
phimunu_DecayTree.Kplus.TupleToolConeIsolation.FillComponents = True
phimunu_DecayTree.Kplus.TupleToolConeIsolation.MaxConeSize = 1.0

phimunu_DecayTree.muplus.addTupleTool("TupleToolConeIsolation")
phimunu_DecayTree.muplus.TupleToolConeIsolation.FillAsymmetry = True
phimunu_DecayTree.muplus.TupleToolConeIsolation.FillDeltas = True
phimunu_DecayTree.muplus.TupleToolConeIsolation.FillComponents = True
phimunu_DecayTree.muplus.TupleToolConeIsolation.MaxConeSize = 1.0
"""

#############################################################################################
#                                                                                           #
#     Create a second DecayTreeTuple from the first which MC tools will be added to         #
#                                                                                           #
#############################################################################################

# phimunu_DecayTree.TupleName='Bu2PhiMuNu'# I will stick with the default DecayTree from now on


if DaVinci().Simulation:
	
	#phimunu_DecayTree.TupleName='Bu2PhiMuNu_MC'# I will stick with the default DecayTree from now on

	from Configurables import MCTupleToolSemileptonic
	phimunu_DecayTree.addTupleTool("TupleToolMCBackgroundInfo")

	MCTruth=phimunu_DecayTree.Bplus.addTupleTool("TupleToolMCTruth")
	MCTruth.addTupleTool("MCTupleToolHierarchy")
	MCTruth.addTupleTool('MCTupleToolSemileptonic')
	
	
	TupleToolIsoGeneric.ToolList += ["TupleToolMCTruth/MCTruth_noniso"]
	TupleToolIsoGeneric.addTool(MCTruth_noniso)

	
DaVinci().UserAlgorithms += [ phimunu_DecayTree ]

