#To be run as  GaudiRun ntuple_options_data.py phimunu_selection.py phimunu_DecayTreeBuilder.py

from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple


# Configure DaVinci

DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Bplus2PhiMuNu_Data.root'
DaVinci().HistogramFile = 'Bplus2PhiMuNu_Data_Hist.root'
DaVinci().PrintFreq = 5000
DaVinci().EvtMax = -1
DaVinci().DataType = '2012'
DaVinci().Simulation = False
DaVinci().Lumi = True

DaVinci().CondDBtag = "default" 
DaVinci().DDDBtag = "default"  

# Use the local input data
#IOHelper().inputFiles([
#	'DST/00049671_00000051_1.semileptonic.dst'
#], clear=True)


