#To be run as  GaudiRun ntuple_optionsrestrip24.py phimunu_restripping.py phimunu_selection.py phimunu_DecayTreeBuilder.py

from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple


# Configure DaVinci
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Bplus2PhiMuNu_MC.root'
DaVinci().HistogramFile = 'Bplus2PhiMuNu_MC_Hist.root'
DaVinci().PrintFreq = 1000
DaVinci().EvtMax = 5000
DaVinci().DataType = '2012'
DaVinci().Simulation = True
DaVinci().Lumi = False

DaVinci().CondDBtag = "" # These options are defined in the ganga file
DaVinci().DDDBtag = ""   # 


#Use the local input data
IOHelper().inputFiles([
#	'./DST/12513010.dst' # B+ ->phi mu nu
#	'./DST/00040080_00000003_2.AllStreams.dst'
#	'./DST/00040080_00000004_2.AllStreams.dst'
#	'./DST/00025425_00000005_1.allstreams.dst' #Bs->Jpsi phi
], clear=True)


