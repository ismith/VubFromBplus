import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import Selection

#############################################################################################
#                                                                                           #
#                              Define Daughter Cuts here:                                   #
#                                                                                           #
#############################################################################################

Mu_Cuts = {	"mu+"	:"(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.3)& (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 )& (MIPCHI2DV(PRIMARY)> 12 )",
		"mu-"	:"(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.3)& (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 )& (MIPCHI2DV(PRIMARY)> 12 )"
}

K_Cuts = {	"K+"	:"(TRCHI2DOF < 2.2 )& (P> 3000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.3)& (PIDK-PIDpi> 0.0 )& (PIDK-PIDp> -2.0 )& (PIDK-PIDmu> -2.0 ) & (MIPCHI2DV(PRIMARY)> 16 )",
		"K-"	:"(TRCHI2DOF < 2.2 )& (P> 3000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.3)& (PIDK-PIDpi> 0.0 )& (PIDK-PIDp> -2.0 )& (PIDK-PIDmu> -2.0 ) & (MIPCHI2DV(PRIMARY)> 16 )"
}


#############################################################################################
#                                                                                           #
#                              Define Combination Cuts here:                                #
#                                                                                           #
#############################################################################################

KK_Comb_Cut = "(AM > 980*MeV) & (AM< 1060.0*MeV)"

PhiMu_Comb_Cut = "(AM>2000.0*MeV) & (AM<5500.0*MeV)"


#############################################################################################
#                                                                                           #
#                              Define Mother Cuts here:                                     #
#                                                                                           #
#############################################################################################


KK_Mother_Cut = "(MM > 990*MeV) & (MM < 1050*MeV) & (VFASPF(VCHI2/VDOF) < 6 ) & (PT > 500.0 *MeV) & (MIPCHI2DV(PRIMARY)> 9 ) & (BPVDIRA> 0.9)"

PhiMu_Mother_Cut = "(MM > 2100*MeV) & (BPVCORRM>2500.0*MeV) & (VFASPF(VCHI2/VDOF)< 3.0) & (BPVDIRA> 0.994)& (BPVVDCHI2 >50.0)"


#############################################################################################
#                                                                                           #
#                              Now start building the selection                             #
#                                                                                           #
#############################################################################################


from Configurables import CombineParticles
_Phi2KK = CombineParticles("Phi2KK",
	DecayDescriptor	= "phi(1020) -> K+ K-",
	DaughtersCuts	= K_Cuts,
	CombinationCut	= KK_Comb_Cut,
	MotherCut		= KK_Mother_Cut
)

from StandardParticles import StdLooseKaons
Sel_Phi2KK = Selection("Sel_Phi2KK", 
                              Algorithm = _Phi2KK, 
                              RequiredSelections = [StdLooseKaons])


_bu2PhiMu = CombineParticles("Bu2PhiMu",
	DecayDescriptor	= "[B+ -> phi(1020) mu+]cc",
	DaughtersCuts	= Mu_Cuts,
	CombinationCut	= PhiMu_Comb_Cut,
	MotherCut		= PhiMu_Mother_Cut
)

from StandardParticles import StdLooseMuons
Sel_bu2PhiMu = Selection("Sel_Bu2PhiMu", 
	Algorithm = _bu2PhiMu, 
	RequiredSelections = [ StdLooseMuons, Sel_Phi2KK ]
)



from Configurables import LoKi__HDRFilter as StripFilter
stripping_filter = StripFilter ( 'StrippingPass' , Code = "HLT_PASS('StrippingB2XuMuNuB2PhiLineDecision')" , Location = "/Event/Strip/Phys/DecReports")

from PhysSelPython.Wrappers import SelectionSequence
phimunu_sequence = SelectionSequence('Seq_Bu2PhiMu', EventPreSelector = [stripping_filter], TopSelection = Sel_bu2PhiMu)

from Configurables import DaVinci, DecayTreeTuple
DaVinci().UserAlgorithms += [ phimunu_sequence.sequence() ]


