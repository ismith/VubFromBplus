from sys import argv

job = int(argv[2])
subjobs = len(jobs(job).subjobs)

file1 = open(argv[1], 'w')

print "from Gaudi.Configuration import *\nfrom GaudiConf import IOHelper\nIOHelper('ROOT').inputFiles(["
file1.write("from Gaudi.Configuration import *\nfrom GaudiConf import IOHelper\nIOHelper('ROOT').inputFiles([\n")
for x in range(subjobs):
	for f in jobs(job).subjobs(x).outputfiles.get("*.xgen"):
		f.localDir = jobs(job).outputdir
		print "\"LFN:" + f.lfn + "\"",
		file1.write("\"LFN:" + f.lfn)
		if x != subjobs-1:
			print ","
			file1.write("\",\n")
		
print "], clear=True)"
file1.write("\"], clear=True)\n")
file1.close()
