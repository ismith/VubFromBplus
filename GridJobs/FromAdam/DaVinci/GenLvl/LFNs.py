from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
"LFN:/lhcb/user/i/ismith/2016_01/125101/125101978/Gauss-13104024-5ev-20160127.xgen",
"LFN:/lhcb/user/i/ismith/2016_01/125101/125101981/Gauss-13104024-5ev-20160127.xgen",
"LFN:/lhcb/user/i/ismith/2016_01/125101/125101985/Gauss-13104024-5ev-20160127.xgen",
"LFN:/lhcb/user/i/ismith/2016_01/125101/125101989/Gauss-13104024-5ev-20160127.xgen",
"LFN:/lhcb/user/i/ismith/2016_01/125101/125101992/Gauss-13104024-5ev-20160127.xgen"
], clear=True)
