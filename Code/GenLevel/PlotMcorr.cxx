#include <string>
#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TLorentzVector.h"
#include "TVector3.h"

#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"

#include "TLatex.h"

void PlotMcorr(std::string s_file = "/afs/cern.ch/user/i/ismith/gangadir/workspace/ismith/LocalXML/291/BpPhiMuNu_Gen.root", std::string bflav = "Bplus", std::string out_name = ""){
	
	
	if ( bflav == "Auto"){ //Try and guess the B flavour from the filename assuming DV000000000.root

		int s_len = int( s_file.length() );
		std::string temp_name = s_file;
		temp_name[s_len-11] = '\0';
		
		std::string B_num;
		B_num.append( &temp_name[s_len - 13] );
		
		std::cout << B_num << std::endl;
		
		if ( B_num.compare("11") == 0)
			bflav = "B0";
		else if (B_num.compare("12") == 0)
			bflav = "Bplus";
		else if (B_num.compare("13") == 0)
			bflav = "B_s0";
		else if (B_num.compare("15") == 0)
			bflav = "Lambda_b0";
		else if (B_num.compare("22") == 0)
			bflav = "D0";
		else if (B_num.compare("23") == 0)
			bflav = "D_splus";
		else
			std::cout << "You're so fucking fucked" << std::endl;
	}
	
	if ( out_name == "Auto"){
		
		int s_len = int( s_file.length() );
		std::string temp_name = s_file;
		temp_name[s_len-5] = '\0';
		
		out_name = &temp_name[s_len-13];
		//std::string B_num;
		//B_num.append( &temp_name[s_len - 13] );
		
		//std::cout << B_num << std::endl;

	}
		
		
		
		
	
	TFile *f_file = TFile::Open( s_file.c_str() );
	TTree *T_tree = (TTree*)f_file->Get("MCDecayTreeTuple/MCDecayTree");
	//TTree *T_tree = (TTree*)f_file->Get("TupleDstToD0pi_D0ToKpi/DecayTree");
	
	TH1F* h_MCORR = new TH1F("h_MCORR", "h_MCORR", 50, 1000, 5600);
	TH1F* h_Phi_M = new TH1F("h_Phi_M", "h_Phi_M", 100, 950, 1090);
	
	
	double B_E = 0;		T_tree->SetBranchAddress( (bflav + "_TRUEP_E").c_str(), &B_E);
	double B_PX = 0;	T_tree->SetBranchAddress( (bflav + "_TRUEP_X").c_str(), &B_PX);
	double B_PY = 0;	T_tree->SetBranchAddress( (bflav + "_TRUEP_Y").c_str(), &B_PY);
	double B_PZ = 0;	T_tree->SetBranchAddress( (bflav + "_TRUEP_Z").c_str(), &B_PZ);
	
	double phi_E = 0;	T_tree->SetBranchAddress("phi_1020_TRUEP_E", &phi_E);
	double phi_PX = 0;	T_tree->SetBranchAddress("phi_1020_TRUEP_X", &phi_PX);
	double phi_PY = 0;	T_tree->SetBranchAddress("phi_1020_TRUEP_Y", &phi_PY);
	double phi_PZ = 0;	T_tree->SetBranchAddress("phi_1020_TRUEP_Z", &phi_PZ);

	double Kp_E = 0;	T_tree->SetBranchAddress("Kplus_TRUEP_E", &Kp_E);
	double Kp_PX = 0;	T_tree->SetBranchAddress("Kplus_TRUEP_X", &Kp_PX);
	double Kp_PY = 0;	T_tree->SetBranchAddress("Kplus_TRUEP_Y", &Kp_PY);
	double Kp_PZ = 0;	T_tree->SetBranchAddress("Kplus_TRUEP_Z", &Kp_PZ);

	double Km_E = 0;	T_tree->SetBranchAddress("Kminus_TRUEP_E", &Km_E);
	double Km_PX = 0;	T_tree->SetBranchAddress("Kminus_TRUEP_X", &Km_PX);
	double Km_PY = 0;	T_tree->SetBranchAddress("Kminus_TRUEP_Y", &Km_PY);
	double Km_PZ = 0;	T_tree->SetBranchAddress("Kminus_TRUEP_Z", &Km_PZ);

	double Mu_E = 0;	T_tree->SetBranchAddress("muplus_TRUEP_E", &Mu_E);
	double Mu_PX = 0;	T_tree->SetBranchAddress("muplus_TRUEP_X", &Mu_PX);
	double Mu_PY = 0;	T_tree->SetBranchAddress("muplus_TRUEP_Y", &Mu_PY);
	double Mu_PZ = 0;	T_tree->SetBranchAddress("muplus_TRUEP_Z", &Mu_PZ);
	
	int nev = T_tree->GetEntries();
	for (int x = 0; x< nev; x++){
		
		T_tree->GetEntry(x);
		
		
		TLorentzVector v_B( B_PX, B_PY, B_PZ, B_E);

		TLorentzVector v_Kp( Kp_PX, Kp_PY, Kp_PZ, Kp_E);
		TLorentzVector v_Km( Km_PX, Km_PY, Km_PZ, Km_E);
		
		TLorentzVector v_Phi( phi_PX, phi_PY, phi_PZ, phi_E);
		
		if (Kp_E != 0)		
			v_Phi = v_Kp + v_Km;
			
		TLorentzVector v_Mu( Mu_PX, Mu_PY, Mu_PZ, Mu_E);
		
		TLorentzVector v_Y = v_Phi + v_Mu;
		
		double M_Y = v_Y.M();
		
		TVector3 V3_B = v_B.Vect();
		TVector3 V3_Y = v_Y.Vect();
		double PT_Y = V3_Y.Perp(V3_B);
		
		double MCorr = sqrt( M_Y * M_Y + PT_Y * PT_Y ) + PT_Y;
		
		h_MCORR->Fill(MCorr);
		h_Phi_M->Fill(v_Phi.M());
		
		if (x%1000 == 0){
			std::cout << x << "  " << B_E << MCorr << "  " <<  std::endl;
			std::cout << Kp_PX<< "  " << Kp_PY << "  " <<Kp_PZ << "  " <<Kp_E << std::endl;
		}
	}
	
	TCanvas* c1 = new TCanvas("c1", "c1", 4800, 1600);
	c1->Divide(2);
	c1->cd(1);
	h_MCORR->GetXaxis()->SetTitle("Corrected Mass [MeV]");
	h_MCORR->Draw();
	c1->cd(2);
	h_Phi_M->GetXaxis()->SetTitle("k^+ K^- mass");
	h_Phi_M->Draw();
	
	if ( out_name != "" ){
		TFile f( ("plots/" + out_name + ".root").c_str(), "new");
		h_MCORR->Scale(1.0 / h_MCORR->Integral() );
		h_MCORR->Write();
		f.Close();
		
		c1->Print(("plots/" + out_name + ".png").c_str());
		c1->Print(("plots/" + out_name + ".C").c_str());
		c1->Print(("plots/" + out_name + ".pdf").c_str());
	}
}

