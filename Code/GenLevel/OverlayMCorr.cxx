#include <string>
#include <iostream>

#include "TFile.h"

#include "TH1.h"
#include "TCanvas.h"
#include "TLegend.h"

#include "TLatex.h"


void OverlayMCorr(){
			
			
	
	vector<std::string> f_names;
	vector<std::string> Decays;
	vector<TH1F*> histos;

	//std::string dir = "/afs/cern.ch/user/i/ismith/NTuples/Vub/VubFromBplus/Backgrounds/GenLevel/" ;
	std::string dir = "plots/" ;

	f_names.push_back( "12513010");	Decays.push_back("B^{+} #rightarrow (#phi #rightarrow #color[2]{K^{+}} #color[2]{K^{-}} )  #color[2]{#mu} #nu");  // Bu -> phi mu nu
	f_names.push_back( "13774011");	Decays.push_back("B_{s} #rightarrow (D_{s} #rightarrow (#phi #rightarrow K^{+} K^{-}) #pi) #mu #nu");   // Bs -> Ds (K K pi) mu nu

	f_names.push_back( "13114006");	Decays.push_back("B_{s} #rightarrow (#phi #rightarrow #color[2]{K^{+}} #color[2]{K^{-}}) #color[2]{#mu} #mu");   // Bs -> phi mu mu
	f_names.push_back( "13144032");	Decays.push_back("B_{s} #rightarrow (J/#psi #rightarrow #color[2]{#mu^{+}} #mu^{-})  (#phi #rightarrow #color[2]{K^{+}} #color[2]{K^{-}})");   // Bs -> J/psi phi
	
	f_names.push_back( "12145052");	Decays.push_back("B^{+} #rightarrow (#phi #rightarrow #color[2]{K^{+} K^{-}}) K #color[2]{#mu} #mu");  	// Bu -> phi K  mu mu
	f_names.push_back( "13146004");	Decays.push_back("B_{s} #rightarrow (#psi(2S) #rightarrow (J/#psi #rightarrow #color[2]{#mu^{+}} #mu^{-}) #pi #pi) (#phi #rightarrow #color[2]{K^{+} K^{-}})");   // Bs -> psi(2s) (J/psi pi pi) phi
	
	
	//These two are a bit too distracting
	//f_names.push_back( "15574010");	Decays.push_back("#Lambda_{b} #rightarrow (#Lambda_{c} #rightarrow p (#phi #rightarrow K^{+} K^{-}) ) #mu #nu ");   // /\b -> /\c (p phi ) mu nu
	//f_names.push_back( "11876001");	Decays.push_back("B^{0} #rightarrow (D_{s} #rightarrow (#phi #rightarrow K^{+} K^{-}) #pi) (D^{*} #rightarrow D^{0} pi)");   // Bd -> Ds(phi pi) Dst ( D pi)

	//These peak too low (probably)
	//f_names.push_back( "22114001");	Decays.push_back("B^{+} #rightarrow #phi #mu #nu");   D0 -> phi mu mu
	//f_names.push_back( "23573002");	Decays.push_back("B^{+} #rightarrow #phi #mu #nu");   Ds -> phi mu nu

	//The following is a duplicate
	//f_names.push_back( "13114001");	Decays.push_back("B^{+} #rightarrow #phi #mu #nu");   // Bs -> phi mu mu

	double maxheight = 0;

	for( auto const& file_name: f_names){
		std::cout << file_name << std::endl;

		TFile* f_1 = TFile::Open( (dir + file_name + ".root").c_str() );
		histos.push_back( (TH1F*)f_1->Get( "h_MCORR") );
		histos.back()->SetName( file_name.c_str() );
		if ( histos.back() -> GetMaximum() > maxheight)
			maxheight = histos.back() -> GetMaximum();
			
	}
					
	TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);
	int it = 0;
	
	for( auto const& hist: histos){
		hist->SetMaximum( maxheight * 1.1 );
		hist->SetLineColor(it+1);
		if(it == 0){
			hist->Draw();
		}
		else
			hist->Draw("SAME");
		
		it++;
	}
	
	histos.at(1)->Draw("SAME");
	histos.at(0)->Draw("SAME");
	
	TLegend *leg_1 = new TLegend(0.2, 0.5, 0.6, 0.8);
	
	for (int h_it = 0; h_it < histos.size(); h_it++){
		leg_1->AddEntry(histos.at(h_it), Decays.at(h_it).c_str(), "lep");
	}
	
	leg_1->Draw();

					
					
}
