#include <string>
#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TH1.h"
#include "TCanvas.h"

#include "TLegend.h"	
#include "TLatex.h"

void SVsBG(	std::string s_file_s = "/afs/cern.ch/user/i/ismith/NTuples/Vub/VubFromBplus/MC/Bu2PhiMuNU_Signal.root", 
				std::string s_file_b = "/afs/cern.ch/user/i/ismith/NTuples/Vub/VubFromBplus/MC/DVntuple.root",
				std::string tree_name  = "TupleBu2phimunu_2/DecayTree" ,
				std::string branch_name = "Bplus_ETA", bool plot = false, bool use_auto = true, double low = 0, double high = 0){

	TFile* TF_file_s = TFile::Open( s_file_s.c_str() );
	TFile* TF_file_b = TFile::Open( s_file_b.c_str() );
	
	TTree* TT_file_s = (TTree*)TF_file_s->Get( tree_name.c_str() );
	TTree* TT_file_b = (TTree*)TF_file_b->Get( tree_name.c_str() );
					
	int n_bins = 100;


	
	if ( use_auto ){
		double low = min( TT_file_s->GetMinimum( branch_name.c_str() ), TT_file_b->GetMinimum( branch_name.c_str() ) );		
		double high = max( TT_file_s->GetMaximum( branch_name.c_str() ), TT_file_b->GetMaximum( branch_name.c_str() ) );
		
		double f_kept = 0.97;
		double nsteps = 200;
		
		double nev_s = TT_file_s->GetEntries();
		double nev_b = TT_file_b->GetEntries();
		
		double nev_pass_s = nev_s;
		double nev_pass_b = nev_b;

		double delta = ( high - low ) / nsteps;
		while  ( nev_pass_s >  f_kept * nev_s and nev_pass_b > f_kept * nev_b ){
			//std::cout<<  "High:  "   << high <<  "  "  << nev_pass_s <<  "  " << nev_s<< std::endl;
			high -= delta;
			nev_pass_s = TT_file_s->GetEntries( (branch_name + " < " + to_string(high)).c_str() );
			nev_pass_b = TT_file_b->GetEntries( (branch_name + " < " + to_string(high)).c_str() );
		}		
		nev_pass_s = nev_s;
		nev_pass_b = nev_b;
		while  ( nev_pass_s > f_kept* nev_s or nev_pass_b > f_kept* nev_b ){
			//std::cout<<  "Low:   "   << low <<  "  "  << nev_pass_s <<  "  " << nev_s<< std::endl;
			low += delta;
			nev_pass_s = TT_file_s->GetEntries( (branch_name + " > " + to_string(low)).c_str() );
			nev_pass_b = TT_file_b->GetEntries( (branch_name + " > " + to_string(low)).c_str() );
		}
		low-=delta;
		high+=delta;

	}
	else if ( !use_auto and low == 0 and high == 0 ){
		double low = min( TT_file_s->GetMinimum( branch_name.c_str() ), TT_file_b->GetMinimum( branch_name.c_str() ) );		
		double high = max( TT_file_s->GetMaximum( branch_name.c_str() ), TT_file_b->GetMaximum( branch_name.c_str() ) );
	}
		

	TH1F* h_S = new TH1F("h_S", "h_S", n_bins, low, high);
	TH1F* h_B = new TH1F("h_B", "h_B", n_bins, low, high);

	TT_file_s->Draw( ( branch_name + ">>h_S" ).c_str() );
	TT_file_b->Draw( ( branch_name + ">>h_B" ).c_str() );

	h_S->Scale( 1 / h_S-> Integral() );
	h_S->SetLineColor(2);
	h_S->SetMarkerColor(2);
	
	h_B->Scale( 1 / h_B-> Integral() );
	
	h_S->SetMaximum( max(h_S->GetMaximum(), h_B->GetMaximum() )*1.1 );
	h_S->GetXaxis()->SetTitle( branch_name.c_str() );
	
	TLegend *leg_1 = new TLegend(0.6, 0.7, 0.8, 0.9);
	
	leg_1->AddEntry(h_S, "B^{+} #rightarrow #phi #mu #nu", "lep");
	leg_1->AddEntry(h_B, "B_{s} #rightarrow J/#psi #phi", "lep");

	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);	
	h_S->Draw();
	h_B->Draw("SAME");
	leg_1->Draw();
	
	if ( plot ){
		c1->Print( ("plots/" + branch_name + ".C" ).c_str() );
		c1->Print( ("plots/" + branch_name + ".pdf"   ).c_str() );
	}


		
}

