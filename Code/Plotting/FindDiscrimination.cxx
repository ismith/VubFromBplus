#include <string>
#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TH1.h"
#include "TCanvas.h"

#include "TLegend.h"	
#include "TLatex.h"

void FindDiscrimination(	std::string s_file_s = "/afs/cern.ch/user/i/ismith/NTuples/Vub/VubFromBplus/MC/Bu2PhiMuNU_Signal.root", 
							std::string s_file_b = "/afs/cern.ch/user/i/ismith/NTuples/Vub/VubFromBplus/MC/DVntuple.root",
							std::string tree_name  = "TupleBu2phimunu_2/DecayTree", double min_dist = 0, bool use_range = false){

	TFile* TF_file_s = TFile::Open( s_file_s.c_str() );
	TFile* TF_file_b = TFile::Open( s_file_b.c_str() );
	
	TTree* TT_file_s = (TTree*)TF_file_s->Get( tree_name.c_str() );
	TTree* TT_file_b = (TTree*)TF_file_b->Get( tree_name.c_str() );
					
	int n_bins = 30;
	
	TH1F* h_dist = new TH1F("h_dist", "h_dist", 100, 0.0, 8);
	
	vector<std::string> particle_names;
	vector<std::string> cone_size_B;
	vector<std::string> cone_size;
	vector<std::string> cone_variable;
	
	particle_names.push_back("Bplus");
	particle_names.push_back("Phi");
	particle_names.push_back("Kplus");
	particle_names.push_back("Kminus");
	particle_names.push_back("muplus");
	
	cone_size_B.push_back("0.50");
	cone_size_B.push_back("1.00");
	cone_size_B.push_back("1.50");
	cone_size_B.push_back("2.00");
	
	cone_size.push_back("0.40");
	cone_size.push_back("0.50");
	cone_size.push_back("0.60");
	cone_size.push_back("0.70");
	cone_size.push_back("0.80");
	cone_size.push_back("0.90");
	cone_size.push_back("1.00");
					
	cone_variable.push_back("cc_mult");		
	cone_variable.push_back("cc_sPT");		
	cone_variable.push_back("cc_vPT");		
	cone_variable.push_back("cc_PX");		
	cone_variable.push_back("cc_PY");		
	cone_variable.push_back("cc_PZ");		
	cone_variable.push_back("cc_asy_P");		
	cone_variable.push_back("cc_asy_PT");		
	cone_variable.push_back("cc_asy_PX");		
	cone_variable.push_back("cc_asy_PY");		
	cone_variable.push_back("cc_asy_PZ");		
	cone_variable.push_back("cc_deltaEta");		
	cone_variable.push_back("cc_deltaPhi");		
	cone_variable.push_back("cc_IT");		
	cone_variable.push_back("cc_maxPt_Q");		
	cone_variable.push_back("cc_maxPt_PT");		
	cone_variable.push_back("cc_maxPt_PX");		
	cone_variable.push_back("cc_maxPt_PY");		
	cone_variable.push_back("cc_maxPt_PZ");		
	cone_variable.push_back("cc_maxPt_PE");		
	cone_variable.push_back("nc_mult");		
	cone_variable.push_back("nc_sPT");		
	cone_variable.push_back("nc_vPT");		
	cone_variable.push_back("nc_PX");		
	cone_variable.push_back("nc_PY");		
	cone_variable.push_back("nc_PZ");		
	cone_variable.push_back("nc_asy_P");		
	cone_variable.push_back("nc_asy_PT");		
	cone_variable.push_back("nc_asy_PX");		
	cone_variable.push_back("nc_asy_PY");		
	cone_variable.push_back("nc_asy_PZ");		
	cone_variable.push_back("nc_deltaEta");		
	cone_variable.push_back("nc_deltaPhi");		
	cone_variable.push_back("nc_IT");		
	cone_variable.push_back("nc_maxPt_PT");		
	cone_variable.push_back("nc_maxPt_PX");		
	cone_variable.push_back("nc_maxPt_PY");		
	cone_variable.push_back("nc_maxPt_PZ");		
	cone_variable.push_back("IT");		
	
	
	vector<std::string> Branch_Names;
	for(std::vector<std::string>::iterator it2 = cone_variable.begin(); it2 != cone_variable.end(); ++it2) {
		for(std::vector<std::string>::iterator it = particle_names.begin(); it != particle_names.end(); ++it) {	
			if (*it == "Bplus"){
				for(std::vector<std::string>::iterator it3 = cone_size_B.begin(); it3 != cone_size_B.end(); ++it3) {
					Branch_Names.push_back( *it + "_" + *it3 + "_" + *it2 );
					//std::cout << *it + "_" + *it3 + "_" + *it2 << std::endl;
				}
			}
			else{
				for(std::vector<std::string>::iterator it3 = cone_size.begin(); it3 != cone_size.end(); ++it3) {
					Branch_Names.push_back( *it + "_" + *it3 + "_" + *it2 );
					//std::cout << *it + "_" + *it3 + "_" + *it2 << std::endl;
				}
			}
		}
	}		
	
	for(std::vector<std::string>::iterator it = Branch_Names.begin(); it != Branch_Names.end(); ++it) {	
		std::string branch_name = *it;

// Find The range to get the discriminant over
		double low  = min( TT_file_s->GetMinimum( branch_name.c_str() ), TT_file_b->GetMinimum( branch_name.c_str() ) );				
		double high = max( TT_file_s->GetMaximum( branch_name.c_str() ), TT_file_b->GetMaximum( branch_name.c_str() ) );

		
		if ( use_range ){
			double f_kept = 0.97;
			double nsteps = 20;
			
			double nev_s = TT_file_s->GetEntries();
			double nev_b = TT_file_b->GetEntries();
			
			double nev_pass_s = nev_s;
			double nev_pass_b = nev_b;
			

			double delta = ( high - low ) / nsteps;
			while  ( nev_pass_s >  f_kept * nev_s and nev_pass_b > f_kept * nev_b ){
				//std::cout<<  "High:  "   << high <<  "  "  << nev_pass_s <<  "  " << nev_s<< std::endl;
				high -= delta;
				nev_pass_s = TT_file_s->GetEntries( (branch_name + " < " + to_string(high)).c_str() );
				nev_pass_b = TT_file_b->GetEntries( (branch_name + " < " + to_string(high)).c_str() );
			}		
			nev_pass_s = nev_s;
			nev_pass_b = nev_b;
			while  ( nev_pass_s > f_kept* nev_s or nev_pass_b > f_kept* nev_b ){
				//std::cout<<  "Low:   "   << low <<  "  "  << nev_pass_s <<  "  " << nev_s<< std::endl;
				low += delta;
				nev_pass_s = TT_file_s->GetEntries( (branch_name + " > " + to_string(low)).c_str() );
				nev_pass_b = TT_file_b->GetEntries( (branch_name + " > " + to_string(low)).c_str() );
			}
			low-=delta;
			high+=delta;
			
		}
	

		TH1F* h_S = new TH1F("h_S", "h_S", n_bins, low, high);
		TH1F* h_B = new TH1F("h_B", "h_B", n_bins, low, high);

		TT_file_s->Draw( ( branch_name + ">>h_S" ).c_str() );
		TT_file_b->Draw( ( branch_name + ">>h_B" ).c_str() );

		h_S->Scale( 1 / h_S-> Integral() );
		h_S->SetLineColor(2);
		h_S->SetMarkerColor(2);
		
		h_B->Scale( 1 / h_B-> Integral() );
		
		h_S->SetMaximum( max(h_S->GetMaximum(), h_B->GetMaximum() )*1.1 );
		h_S->GetXaxis()->SetTitle( branch_name.c_str() );
		
		TLegend *leg_1 = new TLegend(0.6, 0.7, 0.8, 0.9);
		
		leg_1->AddEntry(h_S, "B^{+} #rightarrow #phi #mu #nu", "lep");
		leg_1->AddEntry(h_B, "B_{s} #rightarrow J/#psi #phi", "lep");

	/*	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);	
		h_S->Draw();
		h_B->Draw("SAME");
		leg_1->Draw();
	*/	
		//if ( save ){
		//c1->Print( ("plots/" + branch_name + ".png" ).c_str() );
		//c1->Print( ("plots/" + branch_name + ".C" ).c_str() );
		//c1->Print( ("plots/" + branch_name + ".pdf"   ).c_str() );
		//}


		double dist_pow = 0;
		
		for ( int it = 1; it <= n_bins; it++){
			double cont_S = h_S->GetBinContent(it);
			double cont_B = h_B->GetBinContent(it);
			double dist	= 0;	
			if (cont_S != 0 or cont_B !=0)
				dist = (cont_S - cont_B) / (cont_S + cont_B);
			
			
			dist_pow += abs(dist);
		}
		
		dist_pow = sqrt(dist_pow);
		if (dist_pow > min_dist)			
			std::cout << "Discriminating Power For: " << branch_name << " Is: " << dist_pow << std::endl;
		
		h_dist->Fill(dist_pow);
		
		h_S->Delete();
		h_B->Delete();
	}

	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	h_dist->Draw();
		
}

