// Class: ReadCuts
// Automatically generated by MethodBase::MakeClass
//

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : Cuts::Cuts
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.07/07       [395015]
Creator        : ismith
Date           : Mon Sep 26 15:33:21 2016
Host           : Linux iwan-XPS 3.16.0-71-generic #92~14.04.1-Ubuntu SMP Thu May 12 23:31:46 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
Dir            : /afs/cern.ch/work/i/ismith/ANA/VubFromBplus/MVA/tmva_Bu_V1
Training events: 185791
Analysis type  : [Classification]


#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
H: "False" [Print method-specific help message]
FitMethod: "MC" [Minimisation Method (GA, SA, and MC are the primary methods to be used; the others have been introduced for testing purposes and are depreciated)]
EffMethod: "EffSel" [Selection Method]
# Default:
VerbosityLevel: "Default" [Verbosity level]
VarTransform: "None" [List of variable transformations performed before training, e.g., "D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for the given class of events ('AllClasses' denotes all events of all classes, if no class indication is given, 'All' is assumed)"]
CreateMVAPdfs: "False" [Create PDFs for classifier outputs (signal and background)]
IgnoreNegWeightsInTraining: "False" [Events with negative weights are ignored in the training (but are included for testing and performance evaluation)]
CutRangeMin[0]: "-1.000000e+00" [Minimum of allowed cut range (set per variable)]
    CutRangeMin[1]: "-1.000000e+00"
    CutRangeMin[2]: "-1.000000e+00"
    CutRangeMin[3]: "-1.000000e+00"
    CutRangeMin[4]: "-1.000000e+00"
    CutRangeMin[5]: "-1.000000e+00"
    CutRangeMin[6]: "-1.000000e+00"
CutRangeMax[0]: "-1.000000e+00" [Maximum of allowed cut range (set per variable)]
    CutRangeMax[1]: "-1.000000e+00"
    CutRangeMax[2]: "-1.000000e+00"
    CutRangeMax[3]: "-1.000000e+00"
    CutRangeMax[4]: "-1.000000e+00"
    CutRangeMax[5]: "-1.000000e+00"
    CutRangeMax[6]: "-1.000000e+00"
VarProp[0]: "FSmart" [Categorisation of cuts]
    VarProp[1]: "FSmart"
    VarProp[2]: "FSmart"
    VarProp[3]: "FSmart"
    VarProp[4]: "FSmart"
    VarProp[5]: "FSmart"
    VarProp[6]: "FSmart"
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 7
Kplus_TRCHI2DOF               Kplus_TRCHI2DOF               Kplus_TRCHI2DOF               Kplus_TRCHI2DOF                                                 'F'    [0.186969950795,1.99979174137]
Kminus_TRCHI2DOF              Kminus_TRCHI2DOF              Kminus_TRCHI2DOF              Kminus_TRCHI2DOF                                                'F'    [0.2154083848,1.99998247623]
muplus_TRACK_GhostProb        muplus_TRACK_GhostProb        muplus_TRACK_GhostProb        muplus_TRACK_GhostProb                                          'F'    [0,0.149976208806]
Kplus_TRACK_GhostProb         Kplus_TRACK_GhostProb         Kplus_TRACK_GhostProb         Kplus_TRACK_GhostProb                                           'F'    [0,0.14997933805]
Kminus_TRACK_GhostProb        Kminus_TRACK_GhostProb        Kminus_TRACK_GhostProb        Kminus_TRACK_GhostProb                                          'F'    [2.58366253547e-06,0.149999707937]
Bplus_VCHI2NDOF               Bplus_VCHI2NDOF               Bplus_VCHI2NDOF               Bplus_VCHI2NDOF                                                 'F'    [0.000211338454392,3.99996995926]
Bplus_MissingM2               Bplus_MissingM2               Bplus_MissingM2               Bplus_MissingM2                                                 'F'    [-2998253.5,9739790]
NSpec 0


============================================================================ */

#include <vector>
#include <cmath>
#include <string>
#include <iostream>

#ifndef IClassifierReader__def
#define IClassifierReader__def

class IClassifierReader {

 public:

   // constructor
   IClassifierReader() : fStatusIsClean( true ) {}
   virtual ~IClassifierReader() {}

   // return classifier response
   virtual double GetMvaValue( const std::vector<double>& inputValues ) const = 0;

   // returns classifier status
   bool IsStatusClean() const { return fStatusIsClean; }

 protected:

   bool fStatusIsClean;
};

#endif

class ReadCuts : public IClassifierReader {

 public:

   // constructor
   ReadCuts( std::vector<std::string>& theInputVars ) 
      : IClassifierReader(),
        fClassName( "ReadCuts" ),
        fNvars( 7 ),
        fIsNormalised( false )
   {      
      // the training input variables
      const char* inputVars[] = { "Kplus_TRCHI2DOF", "Kminus_TRCHI2DOF", "muplus_TRACK_GhostProb", "Kplus_TRACK_GhostProb", "Kminus_TRACK_GhostProb", "Bplus_VCHI2NDOF", "Bplus_MissingM2" };

      // sanity checks
      if (theInputVars.size() <= 0) {
         std::cout << "Problem in class \"" << fClassName << "\": empty input vector" << std::endl;
         fStatusIsClean = false;
      }

      if (theInputVars.size() != fNvars) {
         std::cout << "Problem in class \"" << fClassName << "\": mismatch in number of input values: "
                   << theInputVars.size() << " != " << fNvars << std::endl;
         fStatusIsClean = false;
      }

      // validate input variables
      for (size_t ivar = 0; ivar < theInputVars.size(); ivar++) {
         if (theInputVars[ivar] != inputVars[ivar]) {
            std::cout << "Problem in class \"" << fClassName << "\": mismatch in input variable names" << std::endl
                      << " for variable [" << ivar << "]: " << theInputVars[ivar].c_str() << " != " << inputVars[ivar] << std::endl;
            fStatusIsClean = false;
         }
      }

      // initialize min and max vectors (for normalisation)
      fVmin[0] = 0;
      fVmax[0] = 0;
      fVmin[1] = 0;
      fVmax[1] = 0;
      fVmin[2] = 0;
      fVmax[2] = 0;
      fVmin[3] = 0;
      fVmax[3] = 0;
      fVmin[4] = 0;
      fVmax[4] = 0;
      fVmin[5] = 0;
      fVmax[5] = 0;
      fVmin[6] = 0;
      fVmax[6] = 0;

      // initialize input variable types
      fType[0] = 'F';
      fType[1] = 'F';
      fType[2] = 'F';
      fType[3] = 'F';
      fType[4] = 'F';
      fType[5] = 'F';
      fType[6] = 'F';

      // initialize constants
      Initialize();

   }

   // destructor
   virtual ~ReadCuts() {
      Clear(); // method-specific
   }

   // the classifier response
   // "inputValues" is a vector of input values in the same order as the 
   // variables given to the constructor
   double GetMvaValue( const std::vector<double>& inputValues ) const;

 private:

   // method-specific destructor
   void Clear();

   // common member variables
   const char* fClassName;

   const size_t fNvars;
   size_t GetNvar()           const { return fNvars; }
   char   GetType( int ivar ) const { return fType[ivar]; }

   // normalisation of input variables
   const bool fIsNormalised;
   bool IsNormalised() const { return fIsNormalised; }
   double fVmin[7];
   double fVmax[7];
   double NormVariable( double x, double xmin, double xmax ) const {
      // normalise to output range: [-1, 1]
      return 2*(x - xmin)/(xmax - xmin) - 1.0;
   }

   // type of input variable: 'F' or 'I'
   char   fType[7];

   // initialize internal variables
   void Initialize();
   double GetMvaValue__( const std::vector<double>& inputValues ) const;

   // private members (method specific)
   // not implemented for class: "ReadCuts"
};
   inline double ReadCuts::GetMvaValue( const std::vector<double>& inputValues ) const
   {
      // classifier response value
      double retval = 0;

      // classifier response, sanity check first
      if (!IsStatusClean()) {
         std::cout << "Problem in class \"" << fClassName << "\": cannot return classifier response"
                   << " because status is dirty" << std::endl;
         retval = 0;
      }
      else {
         if (IsNormalised()) {
            // normalise variables
            std::vector<double> iV;
            iV.reserve(inputValues.size());
            int ivar = 0;
            for (std::vector<double>::const_iterator varIt = inputValues.begin();
                 varIt != inputValues.end(); varIt++, ivar++) {
               iV.push_back(NormVariable( *varIt, fVmin[ivar], fVmax[ivar] ));
            }
            retval = GetMvaValue__( iV );
         }
         else {
            retval = GetMvaValue__( inputValues );
         }
      }

      return retval;
   }
