#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "Event/Particle.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include "TMath.h"
#include "IsoBDT.C"
#include "TMVAClassification_BDTG_nieces.class.C"
#include "TMVAClassification_BDTG_sisters.class.C"
#include "TMVAClassification_BDTG_allpi.class.C"
#include "TupleToolDmdIsoVars.h"
#include <utility>
#include <iostream>     // std::cout
#include <algorithm>    // std::max
#include <vector>
//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolDmdIsoVars
//
// Author: Basem KHANJI 
//-----------------------------------------------------------------------------
using namespace LHCb ;
using namespace Gaudi::Units;
using namespace ROOT::Math;
//Declaration of Factory
DECLARE_TOOL_FACTORY( TupleToolDmdIsoVars )  //commented to adapt to DecayTreeTuple
//============================================================================
TupleToolDmdIsoVars::TupleToolDmdIsoVars(const std::string& type,
                                         const std::string& name,
                                         const IInterface* parent )
  : TupleToolBase ( type, name , parent ),
  m_dva(0), 
  m_dist(0),
  m_descend(0),
  m_util(0),
  m_vtxfitter(0),
  m_pvReFitterName( "LoKi::PVReFitter:PUBLIC" ),
  //m_pvReFitter("LoKi::PVReFitter:PUBLIC"),
  m_read_BDT_muon(0),
  m_read_BDT_kaon(0),
  m_read_BDT_pion(0),
  m_read_BDT_slpion(0),
  m_read_BDT_muon_New(0),
  m_read_BDT_kaon_New(0),
  m_read_BDT_pion_New(0),
  m_read_BDT_slpion_New(0),
  m_read_BuVeto(0)
  
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("ParticlePath",
                  m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles");
  
}
//=============================================================================
StatusCode TupleToolDmdIsoVars::initialize() {

  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  
  std::vector<std::string> BDT_Var_names;

  m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc(), this ) ;
  if (!m_dva) return Error("Couldn't get parent DVAlgorithm");
  
  m_dist = m_dva->distanceCalculator();
  
  
  m_descend = tool<IParticleDescendants> ( "ParticleDescendants", this );
  if( ! m_descend ) {fatal() << "Unable to retrieve ParticleDescendants tool "<< endreq;
    return StatusCode::FAILURE; }
  
  m_util = tool<ITaggingUtilsChecker> ( "TaggingUtilsChecker", this );
  if( ! m_util ) {fatal() << "Unable to retrieve TaggingUtilsChecker tool "<< endreq;    
    return StatusCode::FAILURE;}
  
  m_pvReFitter = tool<IPVReFitter>( m_pvReFitterName, this );
  if ( !m_pvReFitter )
  {
    return Error( "Unable to retrieve IPVReFitter instance" );
  }
  
  //m_vtxfitter = tool<IVertexFit>("OfflineVertexFitter" , this);
  m_vtxfitter = tool<IVertexFit>("LoKi::VertexFitter");
  if(!m_vtxfitter ) { fatal() << "Unable to retrieve Loki VertexFitter" << endreq;
    return StatusCode::FAILURE; }
  
  BDTvariableNames_muon(m_inNames_muon );
  m_read_BDT_muon = new ReadBDT(m_inNames_muon);
  BDTvariableNames_muon(m_inNames_kaon );
  m_read_BDT_kaon = new ReadBDT(m_inNames_kaon);
  BDTvariableNames_muon(m_inNames_pion );
  m_read_BDT_pion = new ReadBDT(m_inNames_pion);
  BDTvariableNames_muon(m_inNames_slpion );
  m_read_BDT_slpion = new ReadBDT(m_inNames_slpion);
  BDTvariableNames_New_mu(m_inNames_muon_New );
  m_read_BDT_muon_New = new ReadBDTG_sisters(m_inNames_muon_New);
  BDTvariableNames_New(m_inNames_kaon_New );
  m_read_BDT_kaon_New = new ReadBDTG_nieces(m_inNames_kaon_New);
  BDTvariableNames_New(m_inNames_pion_New );
  m_read_BDT_pion_New = new ReadBDTG_nieces(m_inNames_pion_New);
  BDTvariableNames_New_mu(m_inNames_slpion_New );
  m_read_BDT_slpion_New = new ReadBDTG_sisters(m_inNames_slpion_New);
  BuVetovariableNames(m_inNames_BuVeto);
  m_read_BuVeto  = new ReadBDTG_allpi(m_inNames_BuVeto);
  
  m_input_muon = new std::vector<double>;
  m_input_kaon = new std::vector<double>;
  m_input_pion = new std::vector<double>;
  m_input_slpion = new std::vector<double>;
  m_input_muon_New = new std::vector<double>;
  m_input_kaon_New = new std::vector<double>;
  m_input_pion_New = new std::vector<double>;
  m_input_slpion_New = new std::vector<double>;
  m_input_BuVeto = new std::vector<double>;
   
  return sc;
}
//=============================================================================
StatusCode TupleToolDmdIsoVars::fill(const LHCb::Particle* mother 
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple)
{
  
  Assert( P && m_dva
          , "No mother or particle, or tools misconfigured." );
  
  const std::string prefix=fullName(head);
  bool test = true;
  if( !P ) return StatusCode::FAILURE;
  if( !( P->particleID().hasBottom() )) return StatusCode::SUCCESS;
     
  int iso_muon = 0;     double minSumBDT = -100 ;        
  int iso_kaon = 0;     double minSumBDT_nieces = 5.5 ;  
  int iso_pion = 0;     double minSumBDT_sisters = 5.5 ; 
  int iso_slpion = 0;   double minSumBDT_Both = 5.5 ;    
  
  double Dstarpi_M_bestpi = -1;    
  double Best_Dstarpi_VtxChi2 = -1;
  double Best_ratio_distance = -1 ;
  
  const VertexBase* aPV = NULL;
  aPV = m_dva->bestVertex(mother);//bestVertex
    
  Gaudi::XYZPoint PosPV(0,0,200000) ;
  if(!aPV){ return StatusCode::SUCCESS; }
  if(aPV){PosPV = aPV->position(); }
  
  const Vertex* endv = mother->endVertex();
  double Bd_mass = mother->momentum().M();
  
  Gaudi::XYZPoint PosSV(0,0,200000);
  if( !endv ) { return StatusCode::FAILURE; }
  if(  endv ) { PosSV= mother->endVertex()->position(); } 
  
  // Get info from B to estimate the function used in the isolation BDT:
  Gaudi::XYZVector A = mother->momentum().Vect();
  Gaudi::XYZVector B = endv->position() - aPV->position ();
  double cosPFD = A.Dot( B ) / std::sqrt( A.Mag2()*B.Mag2() );
  double Bd_eta = mother->momentum().Eta();
  double Bd_phi = mother->momentum().Phi();
  double Bd_pT  = mother->pt();
  double Sum_of_trackpt = 0;
  double Bd_PointingAngle  = std::acos(cosPFD);
    
  Particle::ConstVector parts_cand = m_descend->descendants(mother);
  Particle::ConstVector parts_cand_clone = m_descend->descendants(mother);
  
  // This is a nasty trick , it is here to make the code runs over D* and D- modes 
  // without having to change the code, in the future one should rewrite 
  // the code (and the isolation tool) to make the tool more compatible with 
  // DecayTreeTuple philosophy : 
  // do not specify the names/id of the particles in the tool
  const Particle* Charm_p = NULL;
  const Particle* Dstar  = findID(413, parts_cand); 
  const Particle* D0     = findID(421, parts_cand);
  const Particle* Dplus  = findID(411, parts_cand); 
  const Particle* muon   = findID(13 , parts_cand);
  const Particle* kaon   = findID(321, parts_cand);
  const Particle* pion   = findID(211, parts_cand, "fast");
  const Particle* slpion = NULL; 
  
  
  if(!muon)   return StatusCode::SUCCESS;
  if(!kaon)   return StatusCode::SUCCESS;
  if(!pion)   return StatusCode::SUCCESS;
  if (Dplus){ 
    Charm_p  = Dplus; 
    D0       = Dplus;  
    parts_cand_clone.erase(std::remove(parts_cand_clone.begin(), parts_cand_clone.end() , pion ) , parts_cand_clone.end());
    slpion   = findID(211, parts_cand_clone,"fast");
  }
  
  else if (!Dstar && D0)
  {
    Charm_p = D0 ;
    slpion = pion ;
  }
  
  else { Charm_p = Dstar; 
    slpion = findID(211, parts_cand, "slow");
  }
  if(!Charm_p) return StatusCode::SUCCESS;
  if(!slpion) return StatusCode::SUCCESS;
  // To adapt this code for both Dstar modes... 
  
  
  // Estimate corrected mass
  double pNuT        = sin(Bd_PointingAngle)*std::sqrt( A.Mag2());
  double corrMass = sqrt(Bd_mass*Bd_mass +pNuT*pNuT) + pNuT;
  
  XYZTVector Dstar4vect;
  Dstar4vect.SetPxPyPzE(Charm_p->momentum().Px(),
                        Charm_p->momentum().Py(),
                        Charm_p->momentum().Pz(),
                        Charm_p->momentum().E() );
  
  double Dstar_eta  = Charm_p->momentum().Eta();
  double Dstar_phi  = Charm_p->momentum().Phi();
  
  double minchi2Dstar = 99999;
  double Dstar_track_VtxChi2 = -1;
  double SumBDT_Forpi = 0;
  
  LHCb::Particle::Range allparts ;
  if (exist<LHCb::Particle::Range>(m_ParticlePath))
  { allparts = get<LHCb::Particle::Range>(m_ParticlePath); }
  else return Warning("Nothing found at "+ m_ParticlePath , StatusCode::SUCCESS,1);
  
  //Fill the tuple                                                                
  //LHCb::Particles* allparts = get<LHCb::Particles>(m_ParticlePath);
  //if (!allparts) err()<<"Entring the loop for signal particles "<<endreq;
  //{std::cout<<"no StdAllNoPIDsPions were found!"<<std::endl; return StatusCode::SUCCESS;}
  //LHCb::Particles::const_iterator im;
  
  LHCb::Particle::Range::const_iterator im;
  for(im = allparts.begin() ; im != allparts.end() ; ++im)
  {
    const Particle * axp  = (*im);
    const Track* track = axp->proto()->track();
    bool isInDecay = false;
    Particle::ConstVector::const_iterator it_p;
    
    for( it_p=parts_cand.begin(); it_p!=parts_cand.end(); it_p++){
      if(!(*it_p)->isBasicParticle()) continue;
      const LHCb::Track* Track_decay = (*it_p)->proto()->track();
      //if(!track || !Track_decay) continue;
      if(Track_decay && track){
        //debug()<<"track of decay particle exist!"<<endreq;
        if(Track_decay == track){
          //debug()<<"This track is in decay .. remove it"<<endreq;
          isInDecay = true;
        }  
      } 
    }
    
    if(isInDecay) continue;
    
    const double track_minIPchi2 = get_MINIPCHI2(axp);
    const double track_IPCHI2_Dst  = get_IPCHI2wrtDcyVtx(Charm_p , axp);
    const double track_D0_distance =  get_D_wrt_DcyVtx(D0, axp);
    //const double track_Dstar_distance =  get_D_wrt_DcyVtx(Charm_p , axp);
    
    //double abs_ratio_distance = -1;
    //if(track_Dstar_distance!=0 && track_D0_distance!=0)
    //{
    //  abs_ratio_distance 
    //    = (std::abs(track_D0_distance)
    //       + std::abs(track_Dstar_distance))/std::abs(track_D0_distance);
    // }
    
    double track_pt = axp->pt();
    double track_ProbNNpi= axp->proto()->info(LHCb::ProtoParticle::ProbNNpi,-0.1) ;
    double track_eta = track->momentum().eta();
    double track_phi = track->momentum().phi();
    double Charge_sign = mother->particleID().pid()/(axp->particleID().pid());
    // Calculate the invariant mass of the track-Dstar pair:
    XYZTVector track4vect;
    track4vect.SetPxPyPzE(
                          axp->momentum().Px(),
                          axp->momentum().Py(),
                          axp->momentum().Pz(),
                          axp->momentum().E()
                          );
    XYZTVector Dstar_pi_mom = Dstar4vect  + track4vect;
    double mass_dstar_pi = std::abs(std::sqrt(Dstar_pi_mom.M2()) );
    //debug()<<" mass (Dstar,pi ) = " << mass_dstar_pi <<endmsg;
    // Vertex the Dstarand the Pion:
    LHCb::Vertex vtx_dstar_pion;
    
    StatusCode Dstapi_fit = m_vtxfitter ->fit( vtx_dstar_pion,*Charm_p, *axp );
    if ( !Dstapi_fit ) { Dstar_track_VtxChi2 = -1; }
        
    else {
      Dstar_track_VtxChi2 = vtx_dstar_pion.chi2()/(double)vtx_dstar_pion.nDoF();
      //debug()<<" chi2 of the Dstar and pion = " <<Dstar_track_VtxChi2<<endreq;
      
      if ( minchi2Dstar> Dstar_track_VtxChi2 && Charge_sign > 0) 
      {
        minchi2Dstar= Dstar_track_VtxChi2   ;
        Best_Dstarpi_VtxChi2 = Dstar_track_VtxChi2;
        Best_ratio_distance =  track_D0_distance;
        Dstarpi_M_bestpi = mass_dstar_pi;
      }
    }
    
    Gaudi::XYZPoint pos_track(track->position());
    Gaudi::XYZVector mom_track(track->momentum());
    Gaudi::XYZPoint pos_muon(muon->proto()->track()->position() );
    Gaudi::XYZVector mom_muon(muon->proto()->track()->momentum());
    
    Gaudi::XYZPoint pos_kaon(kaon->proto()->track()->position());
    Gaudi::XYZVector mom_kaon(kaon->proto()->track()->momentum());
    
    Gaudi::XYZPoint pos_pion(pion->proto()->track()->position());
    Gaudi::XYZVector mom_pion(pion->proto()->track()->momentum());

    Gaudi::XYZPoint pos_slpion(slpion->proto()->track()->position());
    Gaudi::XYZVector mom_slpion(slpion->proto()->track()->momentum());
    // Calculate the input of ISO variable :
    Gaudi::XYZPoint vtx1(0.,0.,0.);
    Gaudi::XYZPoint vtx2(0.,0.,0.);
    Gaudi::XYZPoint vtx3(0.,0.,0.);
    Gaudi::XYZPoint vtx4(0.,0.,0.);
    
    double doca_muon(-1.)  , doca_kaon(-1.)  , doca_pion(-1.)  , doca_slpion(-1.);
    double angle_muon(-1.) , angle_kaon(-1.) , angle_pion(-1.) , angle_slpion(-1.);
    
    InCone(pos_muon   , mom_muon , pos_track , mom_track , vtx1, doca_muon, angle_muon);
    double PVdis1  = ( vtx1.z() -PosPV.z() ) / fabs(vtx1.z()-PosPV.z())*(vtx1-PosPV).R() ;
    double SVdis1   = ( vtx1.z() -PosSV.z() ) / fabs(vtx1.z()-PosSV.z())*(vtx1-PosSV).R() ;
    double fc_mu = pointer(vtx1 -PosPV ,mom_track , mom_muon );
    
    m_input_muon->clear();
    m_input_muon->reserve(6);
    m_input_muon->push_back(track_minIPchi2);
    m_input_muon->push_back(PVdis1);
    m_input_muon->push_back(SVdis1);
    m_input_muon->push_back(doca_muon);
    m_input_muon->push_back(angle_muon);
    m_input_muon->push_back(fc_mu);
    
    double BDT_val_muon =0;
    BDT_val_muon =  m_read_BDT_muon->GetMvaValue( *m_input_muon );
        
    InCone(pos_kaon   , mom_kaon , pos_track , mom_track , vtx2, doca_kaon, angle_kaon);
    double PVdis2  = ( vtx2.z() -PosPV.z() ) / fabs(vtx2.z()-PosPV.z())*(vtx2-PosPV).R() ;
    double SVdis2   = ( vtx2.z() -PosSV.z() ) / fabs(vtx2.z()-PosSV.z())*(vtx2-PosSV).R() ;
    double fc_kaon = pointer(vtx2-PosPV ,mom_track , mom_kaon ) ;
    
    m_input_kaon->clear();
    m_input_kaon->reserve(6);
    m_input_kaon->push_back(track_minIPchi2);
    m_input_kaon->push_back(PVdis2);
    m_input_kaon->push_back(SVdis2);
    m_input_kaon->push_back(doca_kaon);
    m_input_kaon->push_back(angle_kaon);
    m_input_kaon->push_back(fc_kaon);
    double BDT_val_kaon =0;
    BDT_val_kaon =  m_read_BDT_kaon->GetMvaValue( *m_input_kaon );
    //std::cout<<"BDT 2 value = "<< BDT_val_kaon <<std::endl;
        
    InCone(pos_pion   , mom_pion , pos_track , mom_track , vtx3, doca_pion, angle_pion);
    double PVdis3  = ( vtx3.z() -PosPV.z() ) / fabs(vtx3.z()-PosPV.z())*(vtx3-PosPV).R() ;
    double SVdis3  = ( vtx3.z() -PosSV.z() ) / fabs(vtx3.z()-PosSV.z())*(vtx3-PosSV).R() ;
    double fc_pion = pointer(vtx3-PosPV ,mom_track , mom_pion );
    
    m_input_pion->clear();
    m_input_pion->reserve(6);
    m_input_pion->push_back(track_minIPchi2);
    m_input_pion->push_back(PVdis3);
    m_input_pion->push_back(SVdis3);
    m_input_pion->push_back(doca_pion);
    m_input_pion->push_back(angle_pion);
    m_input_pion->push_back(fc_pion);
    double BDT_val_pion =0;
    BDT_val_pion =  m_read_BDT_pion->GetMvaValue( *m_input_pion );
        
    InCone(pos_slpion   , mom_slpion , pos_track , mom_track , vtx4, doca_slpion, angle_slpion);
    double PVdis4  = ( vtx4.z() -PosPV.z() ) / fabs(vtx4.z()-PosPV.z())*(vtx4-PosPV).R() ;
    double SVdis4  = ( vtx4.z() -PosSV.z() ) / fabs(vtx4.z()-PosSV.z())*(vtx4-PosSV).R() ;
    double fc_slpion = pointer(vtx4-PosPV ,mom_track , mom_slpion );
    
    m_input_slpion->clear();
    m_input_slpion->reserve(6);
    m_input_slpion->push_back(track_minIPchi2);
    m_input_slpion->push_back(PVdis4);
    m_input_slpion->push_back(SVdis4);
    m_input_slpion->push_back(doca_slpion);
    m_input_slpion->push_back(angle_slpion);
    m_input_slpion->push_back(fc_slpion);
    double BDT_val_slpion =0;
    BDT_val_slpion =  m_read_BDT_slpion->GetMvaValue( *m_input_slpion );
            
    
    // New BDT tunning:
    // Sisters for slpion and muon
    m_input_muon_New->clear();
    m_input_muon_New->reserve(15);
    m_input_muon_New->push_back(track_minIPchi2);
    m_input_muon_New->push_back(track_ProbNNpi);
    m_input_muon_New->push_back(mass_dstar_pi);
    m_input_muon_New->push_back(track_eta);
    m_input_muon_New->push_back(track_pt);
    m_input_muon_New->push_back(track_IPCHI2_Dst);
    m_input_muon_New->push_back(Dstar_track_VtxChi2);
    m_input_muon_New->push_back(track_D0_distance);
    m_input_muon_New->push_back(Dstar_eta-track_eta);
    m_input_muon_New->push_back(Dstar_phi-track_phi);
    m_input_muon_New->push_back(PVdis1);
    m_input_muon_New->push_back(SVdis1);
    m_input_muon_New->push_back(doca_muon);
    m_input_muon_New->push_back(angle_muon);
    m_input_muon_New->push_back(fc_mu);
    
    double BDT_val_muon_New =0;
    BDT_val_muon_New =  m_read_BDT_muon_New->GetMvaValue( *m_input_muon_New );
    
    m_input_slpion_New->clear();
    m_input_slpion_New->reserve(15);
    m_input_slpion_New->push_back(track_minIPchi2);
    m_input_slpion_New->push_back(track_ProbNNpi);
    m_input_slpion_New->push_back(mass_dstar_pi);
    m_input_slpion_New->push_back(track_eta);
    m_input_slpion_New->push_back(track_pt);
    m_input_slpion_New->push_back(track_IPCHI2_Dst);
    m_input_slpion_New->push_back(Dstar_track_VtxChi2);
    m_input_slpion_New->push_back(track_D0_distance);
    m_input_slpion_New->push_back(Dstar_eta-track_eta);
    m_input_slpion_New->push_back(Dstar_phi-track_phi);
    m_input_slpion_New->push_back(PVdis4);
    m_input_slpion_New->push_back(SVdis4);
    m_input_slpion_New->push_back(doca_slpion);
    m_input_slpion_New->push_back(angle_slpion);
    m_input_slpion_New->push_back(fc_slpion);
    
    double BDT_val_slpion_New =0;
    BDT_val_slpion_New =  m_read_BDT_slpion_New->GetMvaValue( *m_input_slpion_New );
    // Niecies BDT for kaon:
    m_input_kaon_New->clear();
    m_input_kaon_New->reserve(15);
    m_input_kaon_New->push_back(track_minIPchi2);
    m_input_kaon_New->push_back(track_ProbNNpi);
    m_input_kaon_New->push_back(mass_dstar_pi);
    m_input_kaon_New->push_back(track_eta);
    m_input_kaon_New->push_back(track_pt);
    m_input_kaon_New->push_back(track_IPCHI2_Dst);
    m_input_kaon_New->push_back(Dstar_track_VtxChi2);
    m_input_kaon_New->push_back(track_D0_distance);
    m_input_kaon_New->push_back(Dstar_eta-track_eta);
    m_input_kaon_New->push_back(Dstar_phi-track_phi);
    m_input_kaon_New->push_back(PVdis2);
    m_input_kaon_New->push_back(SVdis2);
    m_input_kaon_New->push_back(doca_kaon);
    m_input_kaon_New->push_back(angle_kaon);
    m_input_kaon_New->push_back(fc_kaon);
    
    double BDT_val_kaon_New =0;
    BDT_val_kaon_New =  m_read_BDT_kaon_New->GetMvaValue( *m_input_kaon_New );
    
    // pion also 
    m_input_pion_New->clear();
    m_input_pion_New->reserve(15);
    m_input_pion_New->push_back(track_minIPchi2);
    m_input_pion_New->push_back(track_ProbNNpi);
    m_input_pion_New->push_back(mass_dstar_pi);
    m_input_pion_New->push_back(track_eta);
    m_input_pion_New->push_back(track_pt);
    m_input_pion_New->push_back(track_IPCHI2_Dst);
    m_input_pion_New->push_back(Dstar_track_VtxChi2);
    m_input_pion_New->push_back(track_D0_distance);
    m_input_pion_New->push_back(Dstar_eta-track_eta);
    m_input_pion_New->push_back(Dstar_phi-track_phi);
    m_input_pion_New->push_back(PVdis3);
    m_input_pion_New->push_back(SVdis3);
    m_input_pion_New->push_back(doca_pion);
    m_input_pion_New->push_back(angle_pion);
    m_input_pion_New->push_back(fc_pion);
    
    double BDT_val_pion_New =0;
    BDT_val_pion_New =  m_read_BDT_pion_New->GetMvaValue( *m_input_pion_New );
    
    double SumBDT_sisters = std::min(BDT_val_muon_New , BDT_val_slpion_New);
    double SumBDT_nieces  = std::min(BDT_val_kaon_New , BDT_val_pion_New) ;
    double SumBDT_Both    = std::min(SumBDT_sisters , SumBDT_nieces ) ;
    
    if(  minSumBDT_sisters>SumBDT_sisters  ){
      minSumBDT_sisters = SumBDT_sisters;
    }
    
    if(  minSumBDT_nieces>SumBDT_nieces  ){
      minSumBDT_nieces = SumBDT_nieces;
    }

    if(  minSumBDT_Both >SumBDT_Both  ){
      minSumBDT_Both = SumBDT_Both;
    }
    
    bool IspifromB = false;
    if( minSumBDT_Both< 0  )
    {
      IspifromB = true;
    }
    
    /*std::cout<<" angle_muon = "<< angle_muon <<std::endl;
    std::cout<<" fc_mu = "<< fc_mu <<std::endl;
    std::cout<<" doca_muon = "<< doca_muon <<std::endl;
    std::cout<<" track_minIPchi2 = "<< track_minIPchi2 <<std::endl;
    std::cout<<" SVdis1 = "<< SVdis1 <<std::endl;
    std::cout<<" PVdis1 = "<< PVdis1 <<std::endl;
    */
    // Apply giampie's iso cut:
    if ( angle_muon < 0.27 && fc_mu<0.60 && doca_muon<0.13 && track_minIPchi2 > 3.0 &&
         SVdis1 >-0.15 && SVdis1<30. && PVdis1>0.5 && PVdis1<40. && track->type()==3 ) {
      iso_muon += 1;
      
    }
    if ( angle_kaon < 0.27 && fc_kaon<0.60 && doca_kaon<0.13 &&track_minIPchi2 >3.0 &&
         SVdis2 >-0.15 && SVdis2<30. && PVdis2>0.5 && PVdis2<40. && track->type()==3
         ) {
      iso_kaon += 1;
    }
    if ( angle_pion < 0.27 && fc_pion<0.60 && doca_pion<0.13 &&track_minIPchi2 >3.0 &&
         SVdis3 >-0.15 && SVdis3<30. && PVdis3>0.5 && PVdis3<40. && track->type()==3
      ) {
      iso_pion += 1;
    }
    
    if ( angle_slpion < 0.27 && fc_slpion<0.60 && doca_slpion<0.13 &&track_minIPchi2 >3.0 &&
         SVdis4 >-0.15 && SVdis4<30. && PVdis4>0.5 && PVdis4<40. && track->type()==3
         ) {
      iso_slpion += 1;
    }
    
    double SumBDT = BDT_val_muon + BDT_val_kaon + BDT_val_pion + BDT_val_slpion;
    if( minSumBDT<SumBDT ){
      minSumBDT = SumBDT;
    }
    // SumBDT for pions only 
    if(IspifromB) 
    {
      SumBDT_Forpi += 
        BDT_val_muon + BDT_val_kaon + BDT_val_pion + BDT_val_slpion;
    }
    
    double cone_radius = std::sqrt(std::pow((Bd_eta - track_eta) , 2)  + 
                                   std::pow((Bd_phi - track_phi) , 2)  ) ;
    
    if ( cone_radius <1){
      Sum_of_trackpt += axp->pt() ;
    }
  }
  
  
  double Bd_CDF3 = Bd_pT/(Bd_pT+Sum_of_trackpt);
  
  m_input_BuVeto->clear();
  m_input_BuVeto->reserve(15);
  m_input_BuVeto->push_back(Bd_mass);
  m_input_BuVeto->push_back(Bd_PointingAngle);
  m_input_BuVeto->push_back(Best_Dstarpi_VtxChi2);
  m_input_BuVeto->push_back(Best_ratio_distance);
  m_input_BuVeto->push_back(iso_muon);
  m_input_BuVeto->push_back(iso_kaon);
  m_input_BuVeto->push_back(iso_pion);
  m_input_BuVeto->push_back(iso_slpion);
  m_input_BuVeto->push_back(minSumBDT_sisters);
  m_input_BuVeto->push_back(minSumBDT_nieces);
  m_input_BuVeto->push_back(minSumBDT_Both);
  m_input_BuVeto->push_back(minSumBDT);
  m_input_BuVeto->push_back(Bd_CDF3);
  m_input_BuVeto->push_back(corrMass);
  m_input_BuVeto->push_back(Dstarpi_M_bestpi);
  double BDT_G =0;
  BDT_G =  m_read_BuVeto->GetMvaValue( *m_input_BuVeto);
  
  test &= tuple->column(prefix+"_CorrMass", corrMass);
  test &= tuple->column(prefix+"_PointingAngle"    , Bd_PointingAngle    );
  test &= tuple->column(prefix+"_Best_Dstarpi_VtxChi2", Best_Dstarpi_VtxChi2);
  test &= tuple->column(prefix+"_Best_D0_distance"    , Best_ratio_distance );
  test &= tuple->column(prefix+"_Dstarpi_M_bestpi"    , Dstarpi_M_bestpi    );
  
  test &= tuple->column(prefix+"_iso_muon"  ,iso_muon);
  test &= tuple->column(prefix+"_iso_kaon"  ,iso_kaon);
  test &= tuple->column(prefix+"_iso_pion"  ,iso_pion);
  test &= tuple->column(prefix+"_iso_slpion",iso_slpion);
  
  test &= tuple->column(prefix+"_iso_nieces" , minSumBDT_nieces );
  test &= tuple->column(prefix+"_iso_sisters", minSumBDT_sisters);
  test &= tuple->column(prefix+"_iso_Alldaughters" , minSumBDT_Both);
  test &= tuple->column(prefix+"_iso_SumBDT"     , minSumBDT);
  test &= tuple->column(prefix+"_Bd_CDF3"        , Bd_CDF3);
  test &= tuple->column(prefix+"_BplusVeto"      , BDT_G);
  
  return StatusCode(test);
}

  
const Particle* TupleToolDmdIsoVars::findID(unsigned int id,
                                     Particle::ConstVector& v,
                                     std::string opts )
{
  const Particle* p=0;
  for( Particle::ConstVector::const_iterator ip=v.begin(); ip!=v.end(); ip++)
  {
    if((*ip)->particleID().abspid() == id) 
    {
      if(id==211 && opts=="slow")
      {
        const Particle* mater = m_util->motherof(*ip, v);
        if(mater->particleID().abspid()!=413) continue;
      }
      if(id==211 && opts=="fast")
      {
        const Particle* mater = m_util->motherof(*ip, v);
        if(mater->particleID().abspid()==413) continue;
      }
      p = (*ip);
      break;
    }
  }
 if(!p) 
 {
   return NULL;
 }
 return p;
}

//=============================================================================
double TupleToolDmdIsoVars::get_MINIPCHI2(const Particle* p){  
  double minchi2 = -1 ;
  
  const RecVertex::Range PV = m_dva->primaryVertices();
  if ( !PV.empty() ){
    for (RecVertex::Range::const_iterator pv = PV.begin(); pv!=PV.end(); ++pv){
      double ip, chi2;
      
      RecVertex newPV(**pv);
      StatusCode scfit = m_pvReFitter->remove(p, &newPV);
      if(!scfit) { err()<<"ReFitter fails!"<<endreq; continue; }
      
      LHCb::VertexBase* newPVPtr = (LHCb::VertexBase*)&newPV; 
      bool test2  =  m_dist->distance ( p, newPVPtr, ip, chi2 );
      if( test2 ) {
        if ((chi2<minchi2) || (minchi2<0.)) minchi2 = chi2 ;        
      }
    }
  }
  return minchi2;
}

//========================================================================
double TupleToolDmdIsoVars::get_IPCHI2wrtDcyVtx(const Particle* B,
                                                const Particle* P){
  
  const VertexBase *decayVtxB = B->endVertex();
  double chi2 = 0 ;
  
  if ( 0==decayVtxB )
  { chi2 = -999. ;}
  else 
  {
    double dist = 0;
    bool ok =  m_dist->distance(P , decayVtxB , dist , chi2 );
    if (!ok) return -999.;
  }
  return chi2;
}
//=============================================================================
double TupleToolDmdIsoVars::get_D_wrt_DcyVtx(const Particle* B,
                                      const Particle* P){
  
  const VertexBase *decayVtxB = B->endVertex();
  double chi2 = 0 ;
  double dist = 0;
  if ( 0==decayVtxB )
  {
    chi2 = -999. ;
  }
  else 
  {
    
    bool ok =  m_dist->distance(P , decayVtxB , dist , chi2 );
    if (!ok) return -999.;
  }
  return dist;
}

//=============================================================================
void TupleToolDmdIsoVars::InCone(Gaudi::XYZPoint o1,
                                   Gaudi::XYZVector p1,Gaudi::XYZPoint o2,
                                   Gaudi::XYZVector p2,
                                   Gaudi::XYZPoint& vtx, double&
                                   doca, double& angle) const
{

  Gaudi::XYZPoint rv;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool fail(false);
  closest_point(o1,p1,o2,p2,close,close_mu,vtx, fail);
  if (fail) {
    doca =-1.;
    angle=-1.;
  }
  else {
    doca = (close-close_mu).R();
    angle = arcosine(p1,p2);
  }
}

//============================================================================
double TupleToolDmdIsoVars::pointer (Gaudi::XYZVector vertex,
                                  Gaudi::XYZVector p, Gaudi::XYZVector p_mu) const
{
  double pt=p.Rho()+p_mu.Rho();
  Gaudi::XYZVector ptot(p+p_mu);
  double  num=ptot.R()*sin(arcosine(vertex,ptot));
  double  den=(num+pt);
  double fc = num/den;
  return fc;
}
//============================================================================
void TupleToolDmdIsoVars::closest_point(Gaudi::XYZPoint o,Gaudi::XYZVector p,
                                     Gaudi::XYZPoint o_mu,Gaudi::XYZVector
                                     p_mu, Gaudi::XYZPoint& close1,
                                     Gaudi::XYZPoint& close2,
                                     Gaudi::XYZPoint& vertex, bool&
                                     fail) const
{
  Gaudi::XYZVector v0(o - o_mu);
  Gaudi::XYZVector v1(p.unit());
  Gaudi::XYZVector v2(p_mu.unit());
  Gaudi::XYZPoint temp1(0.,0.,0.);
  Gaudi::XYZPoint temp2(0.,0.,0.);
  fail = false;

  double  d02 = v0.Dot(v2);
  double  d21 = v2.Dot(v1);
  double  d01 = v0.Dot(v1);
  double  d22 = v2.Dot(v2);
  double  d11 = v1.Dot(v1);
  double  denom = d11 * d22 - d21 * d21;
  if (fabs(denom) <= 0.) {
    close1 = temp1;
    close2 = temp2;
    fail = true;
  }
  else {
    double numer = d02 * d21 - d01 * d22;
    double mu1 = numer / denom;
    double mu2 = (d02 + d21 * mu1) / d22;
    close1 = o+v1*mu1;
    close2 = o_mu+v2*mu2;
  }
  vertex = (close1+(close2-close1)*0.5);
}
//============================================================================
double TupleToolDmdIsoVars::arcosine(Gaudi::XYZVector p1,
                                 Gaudi::XYZVector p2) const
{

  double num=(p1.Cross(p2)).R();
  double den=p1.R()*p2.R();
  double seno = num/den;
  double coseno = p1.Dot(p2)/den;
  double alpha = asin(fabs(seno));
  if (coseno < 0 ) {
    alpha = ROOT::Math::Pi() - alpha;
  }
  return alpha; 
}

//============================================================================
void TupleToolDmdIsoVars::BDTvariableNames_muon(std::vector<std::string>& inNames) const {
  inNames.clear();
  inNames.push_back("track_minIPchi2");
  inNames.push_back("track_pvdis_mu");
  inNames.push_back("tracksvdis_mu" );
  inNames.push_back("track_doca_mu" );
  inNames.push_back("track_angle_mu");
  inNames.push_back("track_fc_mu"   );
  return; 
}
//============================================================================
void TupleToolDmdIsoVars::BDTvariableNames_New(std::vector<std::string>& inNames) const {
  inNames.clear();
  inNames.push_back("track_minIPchi2" );
  inNames.push_back("track_ProbNNpi" );
  inNames.push_back("track_dstar_mass" );
  inNames.push_back("track_eta" );
  inNames.push_back("track_pt" );
  inNames.push_back("track_ipchi2_Dst" );
  inNames.push_back("Dstar_track_VtxChi2" );
  inNames.push_back("track_D0_distance" );
  inNames.push_back("deta_track_Dstr" );
  inNames.push_back("dphi_track_Dstr" );
  inNames.push_back("track_pvdis_kaon");
  inNames.push_back("tracksvdis_kaon" );
  inNames.push_back("track_doca_kaon" );
  inNames.push_back("track_angle_kaon");
  inNames.push_back("track_fc_kaon"   );
  return; 
}
//============================================================================
void TupleToolDmdIsoVars::BDTvariableNames_New_mu(std::vector<std::string>& inNames) const {
  inNames.clear();
  inNames.push_back("track_minIPchi2" );
  inNames.push_back("track_ProbNNpi" );
  inNames.push_back("track_dstar_mass" );
  inNames.push_back("track_eta" );
  inNames.push_back("track_pt" );
  inNames.push_back("track_ipchi2_Dst" );
  inNames.push_back("Dstar_track_VtxChi2" );
  inNames.push_back("track_D0_distance" );
  inNames.push_back("deta_track_Dstr" );
  inNames.push_back("dphi_track_Dstr" );
  inNames.push_back("track_pvdis_mu");
  inNames.push_back("tracksvdis_mu" );
  inNames.push_back("track_doca_mu" );
  inNames.push_back("track_angle_mu");
  inNames.push_back("track_fc_mu"   );
  return; 
}
//============================================================================
void TupleToolDmdIsoVars::BuVetovariableNames(std::vector<std::string>& inNames) const {
  
  inNames.clear();
  inNames.push_back("Bd_M" );
  inNames.push_back("Bd_PointingAngle" );
  inNames.push_back("Best_Dstarpi_VtxChi2" );
  inNames.push_back("Best_D0_distance");
  inNames.push_back("iso_muon");
  inNames.push_back("iso_kaon" );
  inNames.push_back("iso_pion" );
  inNames.push_back("iso_slpion" );
  inNames.push_back("iso_min_sisters" );
  inNames.push_back("iso_min_nieces" );
  inNames.push_back("iso_MinAll" );
  inNames.push_back("iso_SumBDT" );
  inNames.push_back("Bd_CDF3" );
  inNames.push_back("missingMass" );
  inNames.push_back("Dstarpi_M_bestpi" );
    
  return; 
}
//====================================================================================

