#ifndef __CINT
#include "RooGlobalFunc.h"
#endif
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include <sstream>
#include "TH1F.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooExponential.h"
#include "RooPolynomial.h"
#include "RooBifurGauss.h"
#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooBinning.h"
#include "RooPlot.h"
#include "TLatex.h"
#include "TAxis.h"
#include "RooStats/SPlot.h"
#include "RooConstVar.h"
using namespace RooStats;
using namespace RooFit;


void sWeight_Bs(int nev = -1);
void FitMC_Bs(int nev = -1);

