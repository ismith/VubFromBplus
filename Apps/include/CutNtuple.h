#include <string>
#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TCut.h"
#include "TChain.h"

/* 
*  Example of how to write a reduced ntuple
*  Two selections, Matts (add on to roadmap):
*  Use: .L ApplySelection.C+
*        ApplySelection("theFile.root")
* Giving extra arguments allows to change ntuple and output name
*        ApplySelection("EtaDown.root", "rhoGammaDown/rhoGammaDown", "_Selected.root");
*/ 


/** First some helpers to avoid duplicating code */

std::string createOutputName(std::string& name, std::string& trailer);

TFile* openOutput(std::string& tree, std::string& input, std::string& output);


void finalize(TTree* cutTree, TFile* output);

/* Now the main business */

std::string CutNtuple(std::string fileName = "mceta.root", std::string cutString  = "", std::string treeName = "EtaGammaGamma", std::string trailer = "_cut.root");

void CutBplus();
