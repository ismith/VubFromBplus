#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TPad.h"
#include "TLine.h"
#include "TLegend.h"



void CompareMCDat(std::string branch_name, int nbins, double low, double high, std::string cuts = " 1 ");
