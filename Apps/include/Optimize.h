
#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TPad.h"
#include "TLine.h"


struct optimise_info{
	std::string name;
	std::string formula;
	
	int nbins;
	double bin_low;
	double bin_high;
	double cut_new_L;
	double cut_new_R;
	double cut_old_L;
	double cut_old_R;	
	
	std::string cutstr_L = "";
	std::string cutstr_R = "";
	
	bool spectator = false;
	
	TH1F* hist_sig;
	TH1F* hist_bg;
	TGraph* punzi_LR;
	TGraph* punzi_RL;
	
	optimise_info(std::string name, std::string formula, int nbins, double bin_low, double bin_high, bool spectator = false): 
		name(name), 
		formula(formula),
		nbins(nbins), 
		bin_low(bin_low), 
		bin_high(bin_high), 
		
		cut_new_L(bin_low), 
		cut_old_L(bin_low),
		
		cut_new_R(bin_high), 
		cut_old_R(bin_high),
		
		spectator(spectator),
		
		hist_sig( new TH1F( (name+"_s").c_str(), name.c_str(), nbins, bin_low, bin_high ) ),
		hist_bg(  new TH1F( (name+"_b").c_str(), name.c_str(), nbins, bin_low, bin_high ) ) {
			
			hist_sig->GetXaxis()->SetTitle(name.c_str() );
			hist_bg->GetXaxis()->SetTitle(name.c_str() );
			
			hist_sig->SetLineColor(kBlue);
			hist_bg ->SetLineColor(kBlack);

			
			}
};

void Optimize(	std::string f_sig, std::string T_sig, std::string f_bg, std::string T_bg, int n_iterations = 5, double punzi_weight = 0.05, double punzi_convergence = 0.5, double punzi_limit = 1.01);

void PlotSigBG( );
