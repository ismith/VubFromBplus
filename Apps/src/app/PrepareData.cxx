#include "CutNtuple.h"
#include <vector>

#include <pthread.h>

#include "GetPrefix.h"

#define NUM_THREADS 2

int thread_check[NUM_THREADS] = {0};


struct thread_data{
	int n;
	std::string file;
	std::string cut;
	std::string tree;
	std::string trailer;
	};


void *ThreadCutNtuple(void *threadarg)
{
	thread_data args = *(struct thread_data *) threadarg;
	thread_check[args.n] = 1;
	//std::cout << "Running Thread with arguments: " << args.file << "  " << args.cut << "  " << args.tree << "  " << args.trailer << std::endl;
	CutNtuple( args.file, args.cut, args.tree, args.trailer );

	thread_check[args.n] = 0;
	pthread_exit(NULL);
}


int main(){
	
	std::vector<std::string> MC_Files;
	std::vector<std::string> Data_Files;
	std::vector<std::string> JPsiPhi_Data;
	
	std::string path_prefix = GetPathPrefix();
	
	MC_Files.push_back( path_prefix + "MC/2011_10010015.root" );
	MC_Files.push_back( path_prefix + "MC/2011_12513010.root" );
	MC_Files.push_back( path_prefix + "MC/2011_13144001.root" );
	//MC_Files.push_back( path_prefix + "MC/2011_13144001_JpsiPhi_Aligned_LooseMu.root" );
	MC_Files.push_back( path_prefix + "MC/2011_13774000.root" );
	
	//Data_Files.push_back(path_prefix + "Data/2011.root");
	
	JPsiPhi_Data.push_back( path_prefix + "Data/2011_JpsiPhi_Aligned_LooseMu.root");
	
	
	std::vector<std::string> AllFiles;
	AllFiles = MC_Files;
	AllFiles.insert( AllFiles.end(), Data_Files.begin(), Data_Files.end() );
	AllFiles.insert( AllFiles.end(), JPsiPhi_Data.begin(), JPsiPhi_Data.end() );
	
	/*
	for( auto& file: AllFiles){
		std::cout << file << std::endl;
	}
	*/
	
	
	


	int rc;
	pthread_t threads[NUM_THREADS];
	pthread_attr_t attr;

	// Initialize and set thread joinable
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	//for( int i=0; i < NUM_THREADS; i++ ){
	for( unsigned int i = 0; i < AllFiles.size(); i++ ){
		
		thread_data* file_info = new thread_data;
		file_info->file = AllFiles[i];
		file_info->cut = "Phi_MM >990 && Phi_MM < 1050";
		file_info->tree = "TupleBu2phimunu/DecayTree";
		file_info->trailer= "_test.root";
		
		
		while ( std::find(std::begin(thread_check), std::end(thread_check), 0) == std::end(thread_check)-1  ){ // Check to see if the number if all threads are busy and if so sleep
			sleep(1); 
		}
		
		sleep(1); // Paranoia to prevent thread unsafe behaviour
		
		int element = std::find(std::begin(thread_check), std::end(thread_check), 0) - thread_check; //find the first element with no thread
		rc = pthread_create(&threads[element], &attr, ThreadCutNtuple, (void *)file_info );
		if (rc){
			std::cout << "Error:unable to create thread," << rc << std::endl;
			exit(-1);
	  }
	}
	
	while ( std::find(std::begin(thread_check), std::end(thread_check), 0) == std::end(thread_check)-1  ){ // Check to see if the number if all threads are busy and if so sleep
		sleep(1); 
	}

	
	void *status;
	pthread_attr_destroy(&attr);
	for( unsigned int i = 0; i < NUM_THREADS; i++ ){
		rc = pthread_join(threads[i], &status);
		if (rc){
			std::cout << "Error:unable to join," << rc << std::endl;
			exit(-1);
		}
		std::cout << "Main: completed thread id :" << i ;
		std::cout << "  exiting with status :" << status << std::endl;
	}

	std::cout << "Main: program exiting." << std::endl;
	pthread_exit(NULL);
	
	
	
	
	
	
	return 0;
	
}
