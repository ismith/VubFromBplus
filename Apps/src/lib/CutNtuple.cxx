#include <string>
#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TCut.h"
#include "TChain.h"

/* 
*  Example of how to write a reduced ntuple
*  Two selections, Matts (add on to roadmap):
*  Use: .L ApplySelection.C+
*        ApplySelection("theFile.root")
* Giving extra arguments allows to change ntuple and output name
*        ApplySelection("EtaDown.root", "rhoGammaDown/rhoGammaDown", "_Selected.root");
*/ 


/** First some helpers to avoid duplicating code */

std::string createOutputName(std::string& name, std::string& trailer){
  // helper for making the output name
  std::string outputName = name.substr(0,name.size() - 5);
  outputName += trailer;
  return outputName;
}

TFile* openOutput(std::string& tree, std::string& input, std::string& output) {
  // helper for opening the file
  TFile* outFile  =new TFile(output.c_str(),"RECREATE");
  std::cout << "Reading: " << tree << " from " << input  << " to " << output << std::endl;
  return outFile;
}

void finalize(TTree* cutTree, TFile* output){
  // helper for finalizing
  TTree*  newtree = cutTree->CloneTree(-1);
  newtree->Write();
  output->Close();
}

/* Now the main business */

std::string CutNtuple(std::string fileName, std::string cutString, std::string treeName, std::string trailer){

  // Matts selection, generally applied on top of the road map
  // get the input
  TChain* decaytree = new TChain(treeName.c_str());
  decaytree->Add(fileName.c_str());
	
  // make the output file name 
  std::string outputName = createOutputName(fileName, trailer);
 
  // make the output file
  TFile* outFile = openOutput(treeName,fileName,outputName); 
 
 
  TCut cut = cutString.c_str();

  TTree* smalltree = decaytree->CopyTree(cut);
  finalize(smalltree,outFile);  

  return outputName;
}


void CutBplus(){
	
	
	//Cut the Monte Carlo:
	CutNtuple("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/MC/MC_2011_14c.root", "Phi_MM >990 && Phi_MM < 1050 && Bplus_MM > 2100 && Bplus_BKGCAT == 50 && Phi_BKGCAT==0", "TupleBu2phimunu/Bu2PhiMuNu_MC", "_SimpleCuts.root");
	//Cut The data
	CutNtuple("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/Data/Data_2011.root", "Phi_MM >990 && Phi_MM < 1050 && Bplus_MM > 2100", "TupleBu2phimunu/Bu2PhiMuNu", "_SimpleCuts.root");
	//Cut The data into Sidebands around the phi
	CutNtuple("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/Data/Data_2011.root", "Phi_MM >990 && Phi_MM < 1050 && Bplus_MM > 2100 && ( Phi_MM < 1010 || Phi_MM > 1035 )", "TupleBu2phimunu/Bu2PhiMuNu", "_SimpleCuts.root");
	
	
	}
