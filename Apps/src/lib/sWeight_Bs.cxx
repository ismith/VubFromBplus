#ifndef __CINT
#include "RooGlobalFunc.h"
#endif
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include <sstream>
#include "TH1F.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooExponential.h"
#include "RooPolynomial.h"
#include "RooBifurGauss.h"
#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooBinning.h"
#include "RooPlot.h"
#include "TLatex.h"
#include "TAxis.h"
#include "RooStats/SPlot.h"
#include "RooConstVar.h"
using namespace RooStats;
using namespace RooFit;


void sWeight_Bs(int nev){
	
	
	std::string in_file = "/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/Data/2011_JpsiPhi_Aligned_LooseMu.root";

	RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
	TFile* F_file = TFile::Open( in_file.c_str() );
	TTree* T_Tree = (TTree*)F_file->Get("TupleBs2JpsiPhi/DecayTree");
	

	int T_Tree_size = T_Tree->GetEntries();

	if( nev == -1 || nev > T_Tree_size)
		nev =  T_Tree_size;


	std::string outputName =in_file.substr(0, in_file.size()-5) + "_s.root";


	TFile* f_out  =new TFile(outputName.c_str(),"RECREATE");
	TTree* smalltree = T_Tree->CopyTree("Bs_MM > 5200 && Bs_MM < 5550", "", nev);
	TTree* newtree = smalltree->CloneTree(-1);


	//Declare Variables for analysis
	RooRealVar m("Bs_MM", "Bs_MM", 5200, 5550);
	
	/*
	TH1F* h_Bs = new TH1F("h1", "h1", 200, 5300, 5450);
	T_Tree->Draw("Bs_MM>>h1");
	RooDataHist data("data", "data", m, h_Bs);
	int ntot = h_Bs->Integral();
	*/
	RooDataSet data("data","data",m,Import(*newtree)) ;
	int ntot = data.sumEntries();


	std::cout<<"Entries: " << ntot << std::endl;
	std::stringstream limit_n;



	//Declare observables
	RooRealVar s_m_mean("s_m_mean", "s_m_mean", 5367.737, 5347., 5387.);
	RooRealVar s_m_sig1("s_m_sig1", "s_m_sig1", 14.899, 5., 70.);
	RooRealVar s_m_sig2("s_m_sig2", "s_m_sig2", 45.839, 5., 70.);



	//Declare PDFs
	RooGaussian    s_m_gaus1("s_m_gaus1", "s_m_gaus1", m, s_m_mean, s_m_sig1);
	RooGaussian    s_m_gaus2("s_m_gaus2", "s_m_gaus2", m, s_m_mean, s_m_sig2);

	RooRealVar     b_m_tau("b_m_tau", "b_m_tau", -2.31e-03, -1e-2, -1e-4);
	RooExponential b_m_exp("b_m_exp", "b_m_exp", m, b_m_tau);
	//RooPolynomial  b_m_exp("b_m__exp", "b_m_exp", m, b_m_tau);

	RooRealVar* Lambda_poly_1 = new RooRealVar("pol_var_1", "pol_var_1", 500, 100, 1000);			//Background Polynomial shape
	RooRealVar* Lambda_poly_2 = new RooRealVar("pol_var_2", "pol_var_2", -0.1, -1.0, -1e-04);			//Background Polynomial shape
	RooPolynomial Lambda_poly = RooPolynomial("poly", "poly", m, RooArgList(*Lambda_poly_1, *Lambda_poly_2));			//Background Polynomial shape

	//Define other observables not directly related to fits
	RooRealVar s_nev("s_nev", "s_nev", 0.9 * ntot, 0, ntot);
	RooRealVar b_nev("b_nev", "b_nev", 0.1 * ntot, 0, ntot);
	//RooRealVar b_nev("b_nev", "b_nev", data.numEntries()*0.1, 0, data.numEntries() );

	RooRealVar s_m_f1("s_m_f1", "s_m_f1", 0.872);//, 0, 1 );
	RooFormulaVar s_m_f2("s_m_f2", "s_m_f2", "1-s_m_f1", RooArgList(s_m_f1) );



	RooAddPdf m_signal("m_signal", "m_signal", RooArgList(s_m_gaus1, s_m_gaus2), RooArgList(s_m_f1, s_m_f2) );
	RooAddPdf m_model("m_model", "m_model", RooArgList(/*s_m_gaus1*/m_signal, b_m_exp/*Lambda_poly*/), RooArgList(s_nev, b_nev) );


	m_model.fitTo(data);//, Extended(), NumCPU(6));



	RooPlot* mframe = m.frame();
	TCanvas *c1 = new TCanvas("c1", "c1",10,44,1600,1600);
	c1->Divide(1,2);
	c1->cd(1);

	data.plotOn(mframe,  Binning(100));
	//m_model.plotOn(mframe, Components(m_signal.GetName() ), LineColor(kRed), LineStyle(2)  );
	m_model.plotOn(mframe, Components(b_m_exp.GetName() ), LineColor(kRed), LineStyle(2)  );
	m_model.plotOn(mframe);
	mframe->GetXaxis()->SetTitle("Mass [MeV]");
	mframe->SetTitle("K^{+} K^{-} #mu^{+} #mu^{-} Invariant Mass");
	mframe->Draw();

	c1->cd(2);

	RooHist* Bspull = mframe->pullHist();
	RooPlot* motherpull = m.frame(Title("Bs Pull Distribution"), Bins(100)) ;
	motherpull->addPlotable(Bspull,"P") ;
	c1->cd(2);
	motherpull->Draw() ;


	c1->Print("sWeight_Bs.pdf");

/*
	TH1F* h_md = new TH1F("h_md", "h_md", 100, 1800, 1940);
	c1->cd(2);
	newtree->Draw("D_M>>h_md");
	h_md->Draw();
*/



	SPlot* sData = new SPlot("sData","An SPlot", data, &m_model, RooArgList(s_nev, b_nev) );



	float Nsig_sw; float Nbkg_sw;  float  L_Nsig; float L_Nbkg; 
	TBranch*  b_Nsig_sw = newtree->Branch("nsig_sw", &Nsig_sw,"nsig_sw/F");  
	TBranch*  b_Nbkg_sw  = newtree->Branch("nback_sw", &Nbkg_sw,"nback_sw/F") ;
	TBranch*  b_L_Nsig  = newtree->Branch("L_nsig", &L_Nsig,"L_nsig/F") ;
	TBranch*  b_L_Nbkg = newtree->Branch("L_nback", &L_Nbkg,"L_nback/F") ;

	for (int i = 0; i < data.numEntries(); ++i) {
		newtree->GetEntry(i);

		const RooArgSet* row = data.get(i);

		Nsig_sw =  row->getRealValue("s_nev_sw");
		L_Nsig =  row->getRealValue("L_s_nev");
		Nbkg_sw =  row->getRealValue("b_nev_sw");
		L_Nbkg =  row->getRealValue("b_nev_sw"); 


		b_Nsig_sw->Fill();
		b_L_Nsig->Fill();
		b_Nbkg_sw->Fill();
		b_L_Nbkg->Fill();

	}

	newtree->Write();
	f_out->Close();  

}


void FitMC_Bs(int nev){
	
	
	std::string in_file = "/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/MC/2011_13144001_JpsiPhi_Aligned_LooseMu.root";

	RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
	TFile* F_file = TFile::Open( in_file.c_str() );
	TTree* T_Tree = (TTree*)F_file->Get("TupleBs2JpsiPhi/DecayTree");
	

	int T_Tree_size = T_Tree->GetEntries();

	if( nev == -1 || nev > T_Tree_size)
		nev =  T_Tree_size;
		
	std::string outputName =in_file.substr(0, in_file.size()-5) + "_s.root";


	TFile* f_out  =new TFile(outputName.c_str(),"RECREATE");
	TTree* smalltree = T_Tree->CopyTree("Bs_MM > 5100 && Bs_MM < 5600", "", nev);
	TTree* newtree = smalltree->CloneTree(-1);


	//Declare Variables for analysis
	RooRealVar m("Bs_MM", "Bs_MM", 5250, 5500);
	RooDataSet data("data","data",m,Import(*newtree)) ;
		


	int ntot = data.numEntries();
	std::cout<<"Entries: " << ntot << std::endl;
	std::stringstream limit_n;



	//Declare observables
	RooRealVar s_m_mean("s_m_mean", "s_m_mean", 5367., 5347., 5387.);
	RooRealVar s_m_sig1("s_m_sig1", "s_m_sig1", 16.8, 5., 50.);
	RooRealVar s_m_sig2("s_m_sig2", "s_m_sig2", 30., 5., 500.);



	//Declare PDFs
	RooGaussian    s_m_gaus1("s_m_gaus1", "s_m_gaus1", m, s_m_mean, s_m_sig1);
	RooGaussian    s_m_gaus2("s_m_gaus2", "s_m_gaus2", m, s_m_mean, s_m_sig2);

	RooRealVar s_m_f1("s_m_f1", "s_m_f1", 0.6, 0, 1 );
	RooFormulaVar s_m_f2("s_m_f2", "s_m_f2", "1-s_m_f1", RooArgList(s_m_f1) );



	RooAddPdf m_signal("m_signal", "m_signal", RooArgList(s_m_gaus1, s_m_gaus2), RooArgList(s_m_f1, s_m_f2) );


	m_signal.fitTo(data);//, Extended(), NumCPU(6));



	RooPlot* mframe = m.frame();
	TCanvas *c1 = new TCanvas("c1", "c1",10,44,1600,1600);
	c1->Divide(1,2);
	c1->cd(1);

	data.plotOn(mframe,  Binning(200));
	m_signal.plotOn(mframe, Components(m_signal.GetName() ), LineColor(kRed), LineStyle(2)  );
	m_signal.plotOn(mframe);
	mframe->GetXaxis()->SetTitle("Mass [MeV]");
	mframe->SetTitle("K^{+} K^{-} #mu^{+} #mu^{-} Invariant Mass");
	mframe->Draw();

	c1->cd(2);

	RooHist* Bspull = mframe->pullHist();
	RooPlot* motherpull = m.frame(Title("Bs Pull Distribution"), Bins(100)) ;
	motherpull->addPlotable(Bspull,"P") ;
	c1->cd(2);
	motherpull->Draw() ;


	f_out->Close();
}

