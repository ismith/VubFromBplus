
#include "Optimize.h"



void Optimize(	std::string f_sig, std::string T_sig_name, std::string f_bg, std::string T_bg_name,
				int n_iterations, double punzi_weight, double punzi_convergence, double punzi_limit){
	
	// n_iterations - How Often should the optimization script be looped
	// punzi_weight - A weight in the punzi equation: eff_s/( weight + sqrt(b) )
	// pnzi_convergence - How rapidly should the cuts converge: cut_new = punzi_convergence * cut_optimal + (1-punzi_convergence) * cut_old
	// punzi_limit - This limit must be passed for a cut to be used.
	
	TFile* f_signal		= TFile::Open( f_sig.c_str() );
	TFile* f_background	= TFile::Open( f_bg.c_str() );
	
	TTree* T_sig = (TTree*)f_signal->Get( T_sig_name.c_str() );
	TTree* T_bg = (TTree*)f_background->Get( T_bg_name.c_str() );
	
	std::string Common_Cuts = "Phi_MM >990 && Phi_MM < 1050 && Bplus_MM > 2100";
	
	std::string cut_sig = " && Bplus_BKGCAT == 50 && Phi_BKGCAT==0";
	std::string cut_bg = " && 1";
	
	double n_sig = T_sig->GetEntries( (Common_Cuts + cut_sig).c_str() );
	double n_bg = T_bg->GetEntries( (Common_Cuts + cut_bg).c_str() );
	
	std::vector<optimise_info> vars;
	
	// The following is an idiotic variable as a workaround to a root bug
	
	vars.push_back( optimise_info("muplus_TRCHI2DOF", "muplus_TRCHI2DOF", 100, 0, 3));
	
	// The following are variables which will be tightened in DaVinci:
	
	vars.push_back( optimise_info("muplus_TRCHI2DOF", "muplus_TRCHI2DOF", 100, 0, 3));
	vars.push_back( optimise_info("Kplus_TRCHI2DOF", "Kplus_TRCHI2DOF", 100, 0, 3));
	vars.push_back( optimise_info("Kminus_TRCHI2DOF", "Kminus_TRCHI2DOF", 100, 0, 3));
	
	//vars.push_back( optimise_info("muplus_P", "muplus_P", 100, 6000, 200000));
	//vars.push_back( optimise_info("Kplus_P", "Kplus_P", 100, 3000, 100000));
	//vars.push_back( optimise_info("Kminus_P", "Kminus_P", 100, 3000, 100000));

	
	//vars.push_back( optimise_info("muplus_PT", "muplus_PT", 100, 1500, 15000));
	//vars.push_back( optimise_info("Kplus_PT", "Kplus_PT", 100, 500, 7500));
	//vars.push_back( optimise_info("Kminus_PT", "Kminus_PT", 100, 500, 7500));
	
	vars.push_back( optimise_info("muplus_TRACK_GhostProb", "muplus_TRACK_GhostProb", 100, 0, 0.1));
	vars.push_back( optimise_info("Kplus_TRACK_GhostProb", "Kplus_TRACK_GhostProb", 100, 0, 0.1));
	vars.push_back( optimise_info("Kminus_TRACK_GhostProb", "Kminus_TRACK_GhostProb", 100, 0, 0.1));	
	
	//vars.push_back( optimise_info("muplus_IPCHI2_OWNPV", "muplus_IPCHI2_OWNPV", 100, 12, 1000));
	//vars.push_back( optimise_info("Kplus_IPCHI2_OWNPV", "Kplus_IPCHI2_OWNPV", 100, 12, 1000));
	//vars.push_back( optimise_info("Kminus_IPCHI2_OWNPV", "Kminus_IPCHI2_OWNPV", 100, 12, 1000));	
		
	vars.push_back( optimise_info("Phi_VCHI2NDOF", "Phi_VCHI2NDOF", 100, 0, 3));
	vars.push_back( optimise_info("Phi_PT", "Phi_PT", 100, 500, 15000));
	vars.push_back( optimise_info("Phi_IPCHI2_OWNPV", "Phi_IPCHI2_OWNPV", 100, 9, 1000));
	vars.push_back( optimise_info("Phi_DIRA_OWNPV", "Phi_DIRA_OWNPV", 100, 0.98, 1));
	
	vars.push_back( optimise_info("Bplus_VCHI2NDOF", "Bplus_VCHI2NDOF", 100, 0, 4));
	vars.push_back( optimise_info("Bplus_DIRA_OWNPV", "Bplus_DIRA_OWNPV", 100, 0.999, 1));
	vars.push_back( optimise_info("Bplus_BPVVDCHI2", "Bplus_BPVVDCHI2", 100, 50, 1000));


	// Now we will add the custom values with cuts:
	//vars.push_back( optimise_info("Kaons Delta_Eta", "abs(Kminus_ETA - Kplus_ETA)", 100, 0, +1.5));
	//vars.push_back( optimise_info("K- mu Delta_Eta", "abs(Kminus_ETA - muplus_ETA)", 100, 0, +1.5));
	//vars.push_back( optimise_info("K+ mu Delta_Eta", "abs(Kplus_ETA - muplus_ETA)", 100, 0, +1.5));	

	//vars.push_back( optimise_info("Bplus_MCORRERR", "Bplus_MCORRERR", 100, 0, 750));
	//vars.push_back( optimise_info("Bplus_MCORRRelErr", "Bplus_MCORRERR/Bplus_MCORR", 100, 0, 0.2));
	//vars.push_back( optimise_info("Bplus_MissingM2", "Bplus_MissingM2", 100, -3e6, 12e6));

	//vars.push_back( optimise_info( "Nu1 Mu dETA", "abs(Bplus_NuSOL1_Eta - muplus_ETA)", 100, 0, 5) );
	//vars.push_back( optimise_info( "Nu2 Mu dETA", "abs(Bplus_NuSOL2_Eta - muplus_ETA)", 100, 0, 5) );


	// Now we will add the spectator variables
	vars.push_back( optimise_info("Phi_MM", "Phi_MM", 100, 990, 1050, true));
	vars.push_back( optimise_info("Bplus_MM", "Bplus_MM", 100, 2100, 5500, true));
	vars.push_back( optimise_info("Bplus_MCORR", "Bplus_MCORR", 100, 4000, 6000, true));
	
	//vars.push_back( optimise_info("Bs Mass", "sqrt( (muplus_NIsoTr_PE + Phi_PE + muplus_PE)**2 - (muplus_NIsoTr_PX + Phi_PX + muplus_PX)**2 - (muplus_NIsoTr_PY + Phi_PY + muplus_PY)**2 - (muplus_NIsoTr_PZ + Phi_PZ + muplus_PZ)**2 )", 100, 5250, 5500, true));
	//vars.push_back( optimise_info("muplus_PAIR_M", "muplus_PAIR_M", 100, 0, 4000, true));

	

	
	
	
	TCanvas* c_plot = new TCanvas("Comparison", "Comparison", 1600, 1600);

	std::string cutstr = "";
	
	
	for (int it = 0; it < n_iterations; it++ ){ 
		
		std::string file_name = "Plots_" + std::to_string(it) + ".pdf";
		
		std::string cutstr_tmp = "";
		c_plot->Print( ( file_name + "(" ).c_str() );
		
		for( auto &var: vars){
			c_plot->cd();
			c_plot->Clear();
			
			//Draw the Histograms and overlay data vs signal:
			
			T_sig->Draw( (var.formula + " >> " + var.name + "_s").c_str(), (Common_Cuts + cut_sig + cutstr).c_str(), "NORM");
			T_bg->Draw( (var.formula+ " >> " + var.name + "_b").c_str(), (Common_Cuts + cut_bg + cutstr).c_str(), "NORM");
			c_plot->Clear();
			
			
			

			
			TH1F* h_temp_s = (TH1F*)var.hist_sig->Clone();
			TH1F* h_temp_b = (TH1F*)var.hist_bg ->Clone();
			
			h_temp_s->Scale( 1. / h_temp_s->Integral() );
			h_temp_b->Scale( 1. / h_temp_b->Integral() );
			
			double h_maximum = std::max( h_temp_s->GetMaximum(), h_temp_b->GetMaximum() ) * 1.05;
			
			
			std::vector<double> punziLR;
			std::vector<double> punziRL;
			std::vector<double> var_valLR;
			std::vector<double> var_valRL;
			double punziRLmax(1), punziLRmax(1);
			double var_valLRmax(0), var_valRLmax(0);
			
			double n_s = var.hist_sig->Integral(0, var.nbins+1);
			double n_b = var.hist_bg->Integral(0, var.nbins+1);
			
			if ( var.spectator){
				h_temp_s->SetMaximum( h_maximum );
			
				h_temp_s->DrawClone();
				h_temp_b->DrawClone("SAME");
				
				c_plot->Update();
				c_plot->Print( file_name.c_str() );
			}
			
			else{
				
				for ( int x = 1; x <= var.nbins; x++ ){
					
					double n_cut_s = var.hist_sig->Integral(x, var.nbins+1) + 1e-10;
					double n_cut_b = var.hist_bg->Integral(x, var.nbins+1) + 1e-10;
					
					double v_punzi = ( n_cut_s / n_s ) / (punzi_weight + sqrt( n_cut_b / n_b ) ) * ( punzi_weight + 1 );
					double var_val = var.bin_low +  ( var.bin_high - var.bin_low ) * (x-0.5) / var.nbins;
					
					punziLR.push_back( v_punzi );
					var_valLR.push_back( var_val );
					
					if ( n_cut_s / n_s < 0.01 )
						continue;
					if ( n_cut_b / n_b < 0.01 )
						continue;
					
					if ( v_punzi > punziLRmax ){
						punziLRmax = v_punzi;
						var_valLRmax = var_val;
					}
					
					
				}
				
				for ( int x = var.nbins; x >= 1; x-- ){
					
					double n_cut_s = var.hist_sig->Integral(0, x) + 1e-10;
					double n_cut_b = var.hist_bg->Integral(0, x) + 1e-10;
					
					double v_punzi = ( n_cut_s / n_s ) / (punzi_weight + sqrt( n_cut_b / n_b ) ) * ( punzi_weight + 1 ) ;
					double var_val = ( var.bin_low + ( var.bin_high - var.bin_low ) * (x-0.5) / var.nbins);
					
					punziRL.push_back( v_punzi );
					var_valRL.push_back( var_val );
					
					if ( n_cut_s / n_s < 0.01 )
						continue;
					if ( n_cut_b / n_b < 0.01 )
						continue;					
					
					if ( v_punzi > punziRLmax ){
						punziRLmax = v_punzi;
						var_valRLmax = var_val;
					}
					
					
				}
				
				if ( punziLRmax > punzi_limit ){

					var.cut_old_L = var.cut_new_L;
					var.cut_new_L = punzi_convergence * var_valLRmax + ( 1- punzi_convergence) * var.cut_old_L;
					
					var.cutstr_L = " && " + var.formula + " > " + std::to_string(var.cut_new_L);
				}
				cutstr_tmp = cutstr_tmp + var.cutstr_L;
				
				if ( punziRLmax > punzi_limit ){

					var.cut_old_R = var.cut_new_R;
					var.cut_new_R = punzi_convergence * var_valRLmax + ( 1-punzi_convergence ) * var.cut_old_R;
						

					var.cutstr_R = " && " + var.formula + " < " + std::to_string(var.cut_new_R);
				}
				cutstr_tmp = cutstr_tmp + var.cutstr_R;
				
				
				//From here it is entierly plotting
				var.punzi_LR = new TGraph(var.nbins, &var_valLR[0], &punziLR[0] );
				var.punzi_RL = new TGraph(var.nbins, &var_valRL[0], &punziRL[0] );
			
				var.punzi_LR->SetLineStyle(2);
				var.punzi_RL->SetLineStyle(2);
				
				var.punzi_LR->SetLineColor(kGreen);
				var.punzi_RL->SetLineColor(kGreen);

				double g_maximum = std::max( punziRLmax, punziLRmax ) * 1.05;
				g_maximum = std::min( g_maximum, 2.5 );
				
				h_temp_s->Scale( g_maximum / h_maximum);
				h_temp_b->Scale( g_maximum / h_maximum);
				
				h_temp_s->SetMaximum( g_maximum );
				
				h_temp_s->Draw();
				h_temp_b->Draw("SAME");
					
				TPad* pad1 = new TPad("pad1", "", 0, 0, 1, 1);
				pad1->SetFillStyle(4000);
				pad1->SetFillColor(0);
				pad1->SetFrameFillStyle(4000);
				
				TLine* line_LR = new TLine(var.cut_new_L, 0, var.cut_new_L, g_maximum);
				line_LR->SetLineColor(kRed);
				
				var.punzi_LR->SetTitle(var.name.c_str());
				var.punzi_LR->DrawClone();
				if ( punziLRmax > punzi_limit )
					line_LR->DrawClone();
				
				TPad* pad2 = new TPad("pad2", "", 0, 0, 1, 1);
				pad2->SetFillStyle(4000);
				pad2->SetFillColor(0);
				pad2->SetFrameFillStyle(4000);
				
				TLine* line_RL = new TLine(var.cut_new_R, 0, var.cut_new_R, g_maximum);
				line_RL->SetLineColor(kRed);

				var.punzi_RL->SetTitle(var.name.c_str());
				var.punzi_RL->DrawClone();
				if ( punziRLmax > punzi_limit )
					line_RL->DrawClone();
				
				
				c_plot->cd();
				pad1->DrawClone();
				pad2->DrawClone();
				c_plot->Update();
				c_plot->Print( file_name.c_str() );

				
			}
			
			
		}
		
		c_plot->Clear();
		c_plot->Print( ( file_name + ")" ).c_str() );
		
		std::cout << cutstr_tmp << std::endl;
		
		cutstr = cutstr_tmp;
		
		double eff_sig = T_sig->GetEntries( (Common_Cuts + cut_sig + cutstr).c_str() ) / n_sig;
		double eff_bg  = T_bg ->GetEntries( (Common_Cuts + cut_bg  + cutstr).c_str() ) / n_bg;
		
		
		std::cout << "Signal Efficiency: " << eff_sig << std::endl;
		std::cout << "Background Efficiency: " << eff_bg << std::endl;
		
		
		
		
	}
}



void PlotSigBG(	std::string f_sig, std::string T_sig_name, std::string f_bg, std::string T_bg_name){
	
	TFile* f_signal = TFile::Open( f_sig.c_str() );
	TFile* f_background = TFile::Open( f_sig.c_str() );
	
	TTree* T_sig = (TTree*)f_signal->Get( T_sig_name.c_str() );
	TTree* T_bg = (TTree*)f_background->Get( T_bg_name.c_str() );
	
	std::string Common_Cuts = "Phi_MM >990 && Phi_MM < 1050 && Bplus_MM > 2100";
	
	std::string cut_sig = " && Bplus_BKGCAT == 50 && Phi_BKGCAT==0";
	std::string cut_bg = " && 1";
	
	double n_sig = T_sig->GetEntries( (Common_Cuts + cut_sig).c_str() );
	double n_bg = T_bg->GetEntries( (Common_Cuts + cut_bg).c_str() );
	
	std::vector<optimise_info> vars;
	
	// The following is an idiotic variable as a workaround to a root bug
	
	vars.push_back( optimise_info("muplus_TRCHI2DOF", "muplus_TRCHI2DOF", 100, 0, 3));
	
	// The following are variables which will be tightened in DaVinci:
	
	
	vars.push_back( optimise_info("muplus_PT", "muplus_PT", 100, 1500, 15000));
	vars.push_back( optimise_info("Kplus_PT", "Kplus_PT", 100, 500, 7500));
	vars.push_back( optimise_info("Kminus_PT", "Kminus_PT", 100, 500, 7500));
	
	vars.push_back( optimise_info("muplus_IPCHI2_OWNPV", "muplus_IPCHI2_OWNPV", 100, 12, 1000));
	vars.push_back( optimise_info("Phi_PT", "Phi_PT", 100, 500, 15000));
	
	
	
	TCanvas* c_plot = new TCanvas("Comparison", "Comparison", 1600, 1600);


		
	std::string file_name = "Plots_SVsBg.pdf";
	
	std::string cutstr_tmp = "";
	c_plot->Print( ( file_name + "(" ).c_str() );
	
	for( auto &var: vars){
		c_plot->cd();
		c_plot->Clear();
		
		//Draw the Histograms and overlay data vs signal:
		
		T_sig->Draw( (var.formula + " >> " + var.name + "_s").c_str(), (Common_Cuts + cut_sig).c_str(), "NORM");
		T_bg->Draw( (var.formula+ " >> " + var.name + "_b").c_str(), (Common_Cuts + cut_bg).c_str(), "NORM");
		c_plot->Clear();
		
		
		

		
		TH1F* h_temp_s = (TH1F*)var.hist_sig->Clone();
		TH1F* h_temp_b = (TH1F*)var.hist_bg ->Clone();
		
		h_temp_s->Scale( 1. / h_temp_s->Integral() );
		h_temp_b->Scale( 1. / h_temp_b->Integral() );
		
		double h_maximum = std::max( h_temp_s->GetMaximum(), h_temp_b->GetMaximum() ) * 1.05;
		
		
		std::vector<double> punziLR;
		std::vector<double> punziRL;
		std::vector<double> var_valLR;
		std::vector<double> var_valRL;
		double punziRLmax(1), punziLRmax(1);
		double var_valLRmax(0), var_valRLmax(0);
		
		double n_s = var.hist_sig->Integral(0, var.nbins+1);
		double n_b = var.hist_bg->Integral(0, var.nbins+1);
		
		h_temp_s->SetMaximum( h_maximum );
	
		h_temp_s->DrawClone();
		h_temp_b->DrawClone("SAME");
		
		c_plot->Update();
		c_plot->Print( file_name.c_str() );

	}

	c_plot->Clear();
	c_plot->Print( ( file_name + ")" ).c_str() );
		

}
