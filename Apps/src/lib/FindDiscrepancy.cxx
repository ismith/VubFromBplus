#include "FindDiscrepancy.h"

void FindDiscrepancy( std::string f_sig, std::string T_sig_name, std::string f_bg, std::string T_bg_name ){
	
	
	TFile* f_signal = TFile::Open( f_sig.c_str() );
	TFile* f_background = TFile::Open( f_bg.c_str() );
	
	TTree* T_sig = (TTree*)f_signal->Get( T_sig_name.c_str() );
	TTree* T_bg = (TTree*)f_background->Get( T_bg_name.c_str() );
	
	std::string Common_Cuts = "Phi_MM >990 && Phi_MM < 1050 && Bplus_MM > 2100 && Bplus_MissingM2 < 1.5e6";
	
	std::string cut_sig = " && Bplus_BKGCAT == 50 && Phi_BKGCAT==0";
	std::string cut_bg = " && 1";	
	
	TCanvas* c1 = new TCanvas("c1", "c1", 800, 600);
	
	double n_sig = T_sig->GetEntries( (Common_Cuts + cut_sig).c_str() );
	double n_bg  = T_bg ->GetEntries( (Common_Cuts + cut_bg ).c_str() );
	
	std::map<double, std::string> discrepancies;
	
	TObjArray *ListOfBranches = T_bg->GetListOfBranches();
	
	for(int i = 0; i < ListOfBranches->GetEntries(); ++i) { 
		std::string branch_name = ListOfBranches->At(i)->GetName();
		
		double d_min = std::max( T_sig->GetMinimum( branch_name.c_str() ), T_bg->GetMinimum( branch_name.c_str() ) );
		double d_max = std::min( T_sig->GetMaximum( branch_name.c_str() ), T_bg->GetMaximum( branch_name.c_str() ) );
		double range = d_max - d_min;
		
		TH1F* h_sig = new TH1F("h_sig", "h_sig", 1000, d_min, d_max);
		TH1F* h_bg  = new TH1F("h_bg",  "h_bg",  1000, d_min, d_max);
		
		T_sig->Draw( (branch_name + " >> h_sig" ).c_str(), (Common_Cuts + cut_sig).c_str() );
		T_bg ->Draw( (branch_name + " >> h_bg"  ).c_str(), (Common_Cuts + cut_bg ).c_str() );
		

		
		if (n_sig == 0 || n_bg == 0 ){
			delete h_sig;
			delete h_bg;
			continue;
		}
		double eff = 1;
		for ( int bin = 1; eff > 0.99; bin++ ){
			
			d_min += range/1000.0 ;
			
			eff = std::min( eff, h_sig->Integral(bin, 1000) / n_sig );
			eff = std::min( eff, h_bg ->Integral(bin, 1000) / n_bg  );

		}
		eff = 1;
		for ( int bin = 1000; eff > 0.99; bin-- ){
			
			d_max -= range/1000.0 ;
			
			eff = std::min( eff, h_sig->Integral(1, bin) / n_sig );
			eff = std::min( eff, h_bg ->Integral(1, bin) / n_bg  );
		}
		
		if ( d_min >= d_max ){
			delete h_sig;
			delete h_bg;
			continue;
		}
		
		delete h_sig;
		delete h_bg;
		h_sig = new TH1F("h_sig", "h_sig", 50, d_min, d_max);
		h_bg  = new TH1F("h_bg",  "h_bg",  50, d_min, d_max);
		
		T_sig->Draw( (branch_name + " >> h_sig" ).c_str(), (Common_Cuts + cut_sig).c_str()  );
		T_bg ->Draw( (branch_name + " >> h_bg"  ).c_str(), (Common_Cuts + cut_bg ).c_str() );
		
		h_bg->SetLineColor(kRed);
		
		c1->Clear();
		
		if (h_sig->Integral() == 0 || h_sig->Integral() == 0 ){
			delete h_sig;
			delete h_bg;
			continue;
		}
		
		
		h_sig->Scale(1/h_sig->Integral() );
		h_bg ->Scale(1/h_bg ->Integral() );
		
		h_sig->GetXaxis()->SetTitle( branch_name.c_str() );
		h_bg->GetXaxis()->SetTitle( branch_name.c_str() );
		
		h_sig->SetMaximum( std::max(h_sig->GetMaximum(), h_bg->GetMaximum() ) * 1.05 );
		
		c1->cd();
		h_sig->Draw();
		h_bg->Draw( "SAME" );
	
		c1->Update();
		
		double Separation_Factor = 0;
		for ( int bin = 1; bin <=50; bin++ ){
			
			Separation_Factor += pow(h_sig->GetBinContent(bin) - h_bg->GetBinContent(bin), 2);
			
		}
		
		//std::cout << setprecision(3) << sqrt(Separation_Factor) << "\t" << branch_name << std::endl;
		
		discrepancies[ Separation_Factor ] = branch_name;
		
		delete h_sig;
		delete h_bg;
	}
	
	for(auto it = discrepancies.begin(); it != discrepancies.end(); it++) {
		
		std::cout << std::scientific << std::setprecision(3) << it->first << "\t" << it->second << std::endl;
		
	}

	
	
}
