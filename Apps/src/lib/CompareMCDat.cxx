#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TPad.h"
#include "TLine.h"
#include "TLegend.h"



void CompareMCDat(std::string branch_name, int nbins, double low, double high, std::string cuts){
	
	
	
	std::string cuts_Bu = "Bplus_BKGCAT == 50 && Phi_BKGCAT==0 && ";
	std::string cuts_Bs = "Bplus_BKGCAT == 50 && Phi_BKGCAT==0 && ";
	std::string cuts_Dat= "Bs_MM > 5330 && Bs_MM< 5410";
	
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	TFile* f_MC_Bu   = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/MC/2011_12513010.root");
	TFile* f_MC_Bs   = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/MC/2011_13144001.root");
	TFile* f_MC_BsJP = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/MC/2011_13144001_JpsiPhi.root");
	TFile* f_Data    = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/Data/2011_JpsiPhi_Aligned_LooseMu_s.root");

	
	TH1F* h_MC_Bu   = new TH1F("h_MC_Bu", "h_MC_Bu", nbins, low, high);
	TH1F* h_MC_Bs   = new TH1F("h_MC_Bs", "h_MC_Bs", nbins, low, high);
	TH1F* h_MC_BsJP = new TH1F("h_MC_BsJP", "h_MC_Bs", nbins, low, high);
	
	TH1F* h_Data = new TH1F("h_Data", "h_Data", nbins, low, high);	
	h_Data->Sumw2();
	
	TTree* T_MC_Bu   = (TTree*)f_MC_Bu  ->Get("TupleBu2phimunu/DecayTree");
	TTree* T_MC_Bs   = (TTree*)f_MC_Bs  ->Get("TupleBu2phimunu/DecayTree");
	TTree* T_MC_BsJP = (TTree*)f_MC_BsJP->Get("TupleBs2JpsiPhi/DecayTree");
	TTree* T_Data    = (TTree*)f_Data   ->Get("DecayTree");
	
	T_MC_Bu  ->Draw( (branch_name + " >>h_MC_Bu"   ).c_str(), ( cuts_Bu + cuts ).c_str());
	T_MC_Bs  ->Draw( (branch_name + " >>h_MC_Bs"   ).c_str(), ( cuts_Bs + cuts ).c_str());
	T_MC_BsJP->Draw( (branch_name + " >>h_MC_BsJP" ).c_str(), ( cuts_Bs + cuts ).c_str());
	T_Data   ->Draw( (branch_name + " >>h_Data"    ).c_str(), ( "(" + cuts_Dat + cuts + ") * nsig_sw"  ).c_str());
	
	//h_MC_Bu->SetFillColor(kRed);
	//h_MC_Bs->SetFillColor(kBlue);
	//h_MC_BsJP->SetFillColor(6);
	h_MC_Bu->SetLineColor(kRed);
	h_MC_Bs->SetLineColor(kBlue);
	h_MC_BsJP->SetLineColor(6);
	//h_MC_Bu->SetFillStyle(3004);
	//h_MC_Bs->SetFillStyle(3005);
	//h_MC_BsJP->SetFillStyle(3344);
	
	double dmax = h_MC_Bu->GetMaximum();
	dmax = std::max( dmax, h_MC_Bs->GetMaximum() / h_MC_Bs->Integral() * h_MC_Bu->Integral() );
	dmax = std::max( dmax, h_MC_BsJP->GetMaximum() / h_MC_BsJP->Integral() * h_MC_Bu->Integral() );
	dmax = std::max( dmax, h_Data->GetMaximum() / h_Data->Integral() * h_MC_Bu->Integral());
	
	
	h_MC_Bu->SetMinimum(0);
	h_MC_Bu->SetMaximum(dmax*1.05);
	h_MC_Bu->GetXaxis()->SetTitle(branch_name.c_str() );
	
	h_MC_Bu->DrawNormalized();
	h_MC_Bs->DrawNormalized("SAME");
	h_MC_BsJP->DrawNormalized("SAME");
	h_Data ->DrawNormalized("SAME");
	
	
	TLegend* leg = new TLegend(0.2,0.7,0.48,0.9);
	//leg->SetHeader("The Legend Title","C"); // option "C" allows to center the header
	leg->AddEntry(h_MC_Bu,"MC: B^{+} #rightarrow #phi #mu^{+} #nu","f");
	leg->AddEntry(h_MC_Bs,"MC: B_{s} #rightarrow J/#psi #phi as B^{+} #rightarrow #phi #mu^{+} #nu","f");
	leg->AddEntry(h_MC_BsJP,"MC: B_{s} #rightarrow J/#psi #phi as B^{+} #rightarrow #phi #mu^{+} #nu + #mu^{-}","f");
	leg->AddEntry(h_Data,"Data: B_{s} #rightarrow J/#psi #phi as B^{+} #rightarrow #phi #mu^{+} #nu + #mu^{-}","lep");
	leg->Draw();

	
	
	
	
	
	}
