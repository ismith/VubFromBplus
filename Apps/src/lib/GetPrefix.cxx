#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cstring>


std::string GetPathPrefix(){
	char name[100];
	gethostname(name, 100);
	name[6]='\0';
	
	std::string file_prefix = "";
	
	if( std::strcmp(name, "lxplus") == 0 ){
		std::cout << "Running on lxplus" << std::endl;
		//file_prefix = "/afs/cern.ch/user/i/ismith/NTuples/RICH_Perf/";
		file_prefix = "/afs/cern.ch/user/i/ismith/NTuples/VubFromBplus/";
	}
	else if( std::strcmp(name, "iwan-X") == 0 ){
		std::cout << "Running on Laptop" << std::endl;
		file_prefix = "/media/ismith/2.0TB_Drive/NTuples/VubFromBplus/";
	}
	else
		return "";
	return file_prefix;
		
}
